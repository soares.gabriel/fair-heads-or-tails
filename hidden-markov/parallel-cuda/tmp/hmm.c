/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    February 23, 2011
 * @brief   Implements several important functions for Hidden Markov Models
 */
 
#include <stdio.h>
#include <stdlib.h>
 
#include "hmm.h"
 

/* Print the parameters of the HMM (A, B, Pi) to the console */
void print_hmm(Hmm *hmm)
{
 
    int j;
    int k;
    int n_st = hmm->nstates;
    int n_sy = hmm->nsymbols;
    
    /* Print A */
    printf("# state transition probability (A)\n");
    for (j = 0; j < n_st; j++) {
        for (k = 0; k < n_st; k++) {
            printf(" %.4f", hmm->a[(j * n_st) + k]);
        }
        printf("\n");
    }
    
    /* Print B */
    printf("# state output probility (B)\n");
    for (j = 0; j < n_sy; j++) {
        for (k = 0; k < n_st; k++) {
            printf(" %.4f", hmm->b[(j * n_st) + k]);
        }
        printf("\n");
    }
    
    /* Print Pi */
    printf("# initial state probability (Pi)\n");
    for (j = 0; j < n_st; j++) {
        printf(" %.4f", hmm->pi[j]);
    }
    printf("\n\n");
}

 
/* Print the observation sequence to the console */
void print_obs(Obs *obs)
{
    int j;
    printf("Observation sequence:\n");
    for (j = 0; j < obs->length; j++) {
        printf(" %i", obs->data[j]);
    }
    printf("\n\n");
}


/* Free the memory associated with the HMM and observation sequence */
void free_vars(Hmm *hmm, Obs *obs)
{
    if (hmm != NULL) {
        if (hmm->a != NULL) {
            free(hmm->a);
        }
        if (hmm->b != NULL) {
            free(hmm->b);
        }
        if (hmm->pi != NULL) {
            free(hmm->pi);
        }
        free(hmm);
    }
    if (obs != NULL) {
        if (obs->data != NULL) {
            free(obs->data);
        }
        free(obs);
    }
}
