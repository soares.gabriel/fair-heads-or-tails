/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    March 26, 2011
 * @brief   Implements the Backward Algorithm (FO) in CUDA
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <cuda.h>
//#include <cutil.h>

#include "hmm.h"
extern "C" {                    /* required for functions to be visible in C */
#include "hmm_fo_cu.h"
}

/* Contains information about the graphics card's CUDA capabilities */
enum {
    THREADS_PER_BLOCK = 1024     /* Note: must be a power of 2 for reductions */
};

__device__ float c_d[THREADS_PER_BLOCK * THREADS_PER_BLOCK];


/******************************************************************************/
/* CUDA - Initialize backward variables (beta) */
__global__ void init_beta_dev(  float *beta_d, int nstates, float scale)
{
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx < nstates) {
        beta_d[idx] = 1.0f / scale;
    }
}


/******************************************************************************/
/* CUDA - Calculate beta variables */
__global__ void calc_beta_dev(  float *beta_d,
                                float *a_d,
                                float *b_d,
                                int nstates,
                                int obs_t,
                                int t,
                                float scale)
{
    unsigned int tid = threadIdx.x;
    //unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int bid = blockIdx.x;
    unsigned int stride;
    __shared__ float partial_sum[THREADS_PER_BLOCK];


    if (tid < nstates && bid < nstates)  {
        c_d[(bid * nstates) + tid] = beta_d[((t-1) * nstates) + tid] *
                                        a_d[(tid * nstates) + bid] *
                                        b_d[((obs_t+1) * nstates) + bid];
    }
    /* Fill partial sums in each block */
    if (tid < nstates && bid < nstates) {
        partial_sum[tid] = c_d[(bid * nstates) + tid];
    } else {
        partial_sum[tid] = 0;
    }
    
    /* Perform reduction on partial sum in each block */
    __syncthreads();
    for (stride = blockDim.x/2; stride > 0; stride /= 2) {
        if (tid < stride) {
            partial_sum[tid] = partial_sum[tid] + partial_sum[tid + stride];
        }
        __syncthreads();
    }
    
    /* Copy sums to alpha */
    if (tid == 0 && bid < nstates) {
        beta_d[(t * nstates) + bid] = partial_sum[0];
    }
}

/******************************************************************************/
/* Runs the backward algorithm on the supplied HMM and observation sequence */
int main()
{
    /* Host-side variables */
    int size, j , k;
    int nsymbols = 2;
    int nstates = 2;
    float *beta;           /* TODO: Needed? */
    float *scale;           /* TODO: Needed? */
    float *a = (float*)malloc(sizeof(float)*nstates*nstates);
    float *b = (float*)malloc(sizeof(float)*nstates*nsymbols);
    float *pi = (float*)malloc(sizeof(float)*nstates);
printf("# state transition probability (A)\n");
    for (j = 0; j < nstates; j++) {
        for (k = 0; k < nstates; k++) {
		a[(j * nstates) + k] = 0.5;
            printf(" %.4f", a[(j * nstates) + k]);
        }
        printf("\n");
    }
    printf("\n\n");
    /* Print B */
    printf("# state output probility (B)\n");
    for (j = 0; j < nsymbols; j++) {
        for (k = 0; k < nstates; k++) {
		b[(j * nstates) + k] = 0.5;
            printf(" %.4f", b[(j * nstates) + k]);
        }
        printf("\n");
    }
    printf("\n\n");
    /* Print Pi */
    printf("# initial state probability (Pi)\n");
    for (j = 0; j < nstates; j++) {
		pi[j] = 0.5;
        printf(" %.4f", pi[j]);
    }
    printf("\n\n");

    int length = 4;
    int *obs = (int*)malloc(sizeof(int)*length);
    printf("# observations sequence (Obs)\n");
    for (j = 0; j < length; j++) {
                obs[j] = 0;
        printf(" %d", obs[j]);
    }
    printf("\n\n");
    int threads_per_block = THREADS_PER_BLOCK;
    int nblocks;
    int t;
    float log_lik;

    /* Device-side variables */
    float *a_d;
    float *b_d;
    float *pi_d;
    float *obs_d;
    float *beta_d;
    float *scale_d;
    float *log_lik_d;

    /* TODO: Needed? */
    beta = (float *) malloc(sizeof(float) * nstates * length);
    scale = (float *) malloc(sizeof(float) * length);

    /* Check for number of states against maximum allowed */
    if (nstates > THREADS_PER_BLOCK) {
        return 1.0f;
    }

    /* Allocate memory and copy needed parameters to device */
    size = sizeof(float) * nstates * nstates;
      cudaMalloc((void**)&a_d, size) ;
      cudaMemcpy(a_d, a, size, cudaMemcpyHostToDevice) ;
    size = sizeof(float) * nstates * nsymbols;
      cudaMalloc((void**)&b_d, size) ;
      cudaMemcpy(b_d, b, size, cudaMemcpyHostToDevice) ;
    size = sizeof(float) * nstates;
      cudaMalloc((void**)&pi_d, size);
      cudaMemcpy(pi_d, pi, size, cudaMemcpyHostToDevice) ;
    size = sizeof(float) * length;
      cudaMalloc((void**)&obs_d, size);
      cudaMemcpy(obs_d, obs, size, cudaMemcpyHostToDevice) ;
    size = sizeof(float) * nstates * length;
      cudaMalloc((void**)&beta_d, size) ;
    size = sizeof(float) * length;
      cudaMalloc((void**)&scale_d, size) ;
    size = sizeof(float);
      cudaMalloc((void**)&log_lik_d, size) ;

    /* Initialize beta variables */
    init_beta_dev<<<1, threads_per_block>>>(  beta_d,
                                                nstates,
                                                1.0f);

    /* Calculate the rest of the beta variables */
    nblocks = threads_per_block;
    for (t = length; t > 0; t--) {
        calc_beta_dev<<<nblocks, threads_per_block>>>( beta_d,
                                                        a_d,
                                                        b_d,
                                                        nstates,
                                                        obs[t],
                                                        t,
                                                        1.0f);
    }

    /* Copy results from device to host */
    size = sizeof(float) * nstates * length;
      cudaMemcpy(beta, beta_d, size, cudaMemcpyDeviceToHost);
//    size = sizeof(float) * length;
//      cudaMemcpy(scale, scale_d, size, cudaMemcpyDeviceToHost) ;


printf("# beta\n");
    for (j = 0; j < length; j++) {
        for (k = 0; k < nstates; k++) {
            printf(" %.4f", beta[(j * nstates) + k]);
        }
        printf("\n");
    }

    /* Free device memory */
      cudaFree(a_d) ;
      cudaFree(b_d) ;
      cudaFree(pi_d) ;
      cudaFree(obs_d) ;
      cudaFree(beta_d) ;
      cudaFree(scale_d) ;

//    return log_lik;
	return 0;
}
