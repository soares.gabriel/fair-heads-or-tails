/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    June 2, 2011
 * @brief   Tests the Viterbi Algorithm in CUBLAS
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../main/hmm.h"
#include "../main/hmm_test.h"
#include "../main/hmm_vit_cublas.h"
 
/* Read in HMM and observation from file and test Viterbi */ 
int main(int argc, char *argv[]) {

    /* Initialize variables */
    int c;
    Hmm *in_hmm;                /* Initial HMM */
    Obs *in_obs;                /* Observation sequence */
    int *state_seq;
    char *configfile = NULL;
    float log_lik;              /* Output likelihood of FO */
    struct timeval timer;
    int t;
    
    /* Initialize HMM and observation sequence */
    in_hmm = (Hmm *)malloc(sizeof(Hmm));
    in_obs = (Obs *)malloc(sizeof(Obs));
    
    /* Parse CLI arguments */
    while ((c = getopt(argc, argv, "c:n:")) != -1) {
        switch(c) {
        
            /* Define config file */
            case 'c':
                configfile = optarg;
                break;
            
            /* Define case for unknown argument */
            case '?':
                fprintf(stderr, "illegal options\n");
                exit(EXIT_FAILURE);
                
            /* Define default case */
            default:
                abort();
        }
    }
    
    /* Read HMM paramters and observation sequence from file */
    read_hmm_file(configfile, in_hmm, in_obs, 0);

    /* Print initial HMM paramters as extracted from the config file */
    printf("\n");
    printf("HMM parameters read from file:\n");
    print_hmm(in_hmm);
    
    /* Run the Viterbi Algorithm on the HMM and observation sequence */
    state_seq = (int *)malloc(sizeof(int) * in_obs->length);
    tic(&timer);
    run_hmm_vit(in_hmm, in_obs, state_seq);
    toc(&timer);
    printf("seconds\n");
    
    /* Print state sequence */
    printf("State sequence:\n");
    for (t = 0; t < in_obs->length; t++) {
        printf(" %i", state_seq[t]);
    }
    printf("\n\n");
    
    /* Free memory */
    free_vars(in_hmm, in_obs);
    free(state_seq);
    
    return 0;
}

