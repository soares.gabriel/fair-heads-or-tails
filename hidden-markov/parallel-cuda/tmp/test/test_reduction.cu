/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    March 24, 2011
 * @brief   Tests algorithm to perform a reduction across multiple blocks
 *
 * This handles multiple block reductions without multiple kernel calls.
 *
 * First, break array into shared memory (partial sum) and reduce the partial
 * sums. Then, copy the partial sums to a single array (we will re-use the
 * partial sum in the first block), and reduce that.
 *
 * Note: This technique is limited to a maximum block size equal to the number
 * of threads per block. Also, the number of threads per block needs to be a
 * power of 2 (32, 64, 128, etc.).
 *
 * @todo This does not work. Need to add a lock after __threadfence() and have
 * the last calling block perform the final reduction.
 */

#include <stdio.h>
#include <stdlib.h>

#include <cuda.h>
#include <cutil.h>

enum {
    THREADS_PER_BLOCK = 128,        /**< Use constant threads per block */
    MAX_BLOCKS = THREADS_PER_BLOCK, /**< Need max blocks = threads per block */
    NUM_ELEMENTS = 10000            /**< Change this to set size of array */
};

__device__ float *global_sums_d;    /**< Global container for local sums */

/******************************************************************************/
/* CUDA - reduction - sum is in array_d[0] */
__global__ void test_reduction(float *array_d, int nblocks)
{
    unsigned int tid = threadIdx.x;             /* Thread ID in each block */
    unsigned int bid = blockIdx.x;              /* Block ID in each grid */
    unsigned int idx = bid * blockDim.x + tid;  /* Process ID in each grid */
    unsigned int stride;
    __shared__ float partial_sum[THREADS_PER_BLOCK];
    
    /* Clear global memory */
    if (idx < blockDim.x) {
        global_sums_d[idx] = 0;
    }
    
    /* Fill partial sum array */
    if (idx < NUM_ELEMENTS) {
        partial_sum[tid] = array_d[idx];
    } else {
        partial_sum[tid] = 0;
    }
    
    /* Perform reduction on partial sum */
    __syncthreads();
    for (stride = blockDim.x/2; stride > 0; stride /= 2) {
        if (tid < stride) {
            partial_sum[tid] = partial_sum[tid] + partial_sum[tid + stride];
        }
        __syncthreads();
    }
    
    /* Copy local partial sums to global memory */
    if (tid == 0) {
        global_sums_d[bid] = partial_sum[0];
    }
    __threadfence_block();    /* Makes sure global memory writes are visible */
    
    /* Copy partial sums to first block's partial sum array */
    __syncthreads();
    if (bid == 0 && tid < nblocks) {
        partial_sum[tid] = global_sums_d[tid];
    } else {
        partial_sum[tid] = 0;
    }
    
    /* Perform reduction again */
    __syncthreads();
    for (stride = blockDim.x/2; stride > 0; stride /= 2) {
        if (tid < stride) {
            partial_sum[tid] = partial_sum[tid] + partial_sum[tid + stride];
        }
        __syncthreads();
    }
    
    /* Copy partial sum value to array */
    if (bid == 0) {//if (idx == 0) {
        array_d[0] = partial_sum[0];
    }
}

/******************************************************************************/
/* Main */
int main(int argc, char *argv[]) {

    int nelements = NUM_ELEMENTS;
    float *array;
    float *array_d;
    float sum;
    int size;
    int i;
    int threads_per_block = THREADS_PER_BLOCK;
    int nblocks = (nelements + threads_per_block - 1) / threads_per_block;
    
    /* Fill array */
    array = (float *) malloc(sizeof(float) * nelements);
    for (i = 0; i < NUM_ELEMENTS; i++) {
        array[i] = 1;
    }
    
    /* Print CUDA paramters */
    printf("\n");
    printf("CUDA threads per block: %i\n", threads_per_block);
    printf("CUDA blocks launched: %i\n", nblocks);
    printf("\n");
    
    /* Check to make sure the number of blocks isn't over the max */
    if (nblocks > MAX_BLOCKS) {
        printf("Error: trying to launch too many blocks!\n");
        return 0;
    }
                                                
    /* Find sum via CPU */
    sum = 0;
    for (i = 0; i < nelements; i++) {
        sum = sum + array[i];
    }
    printf("CPU sum: %.4f\n", sum);
    
    /* Find sum via GPU */
    size = sizeof(float) * threads_per_block;
    CUDA_SAFE_CALL( cudaMalloc((void**)&global_sums_d, size) );
    size = sizeof(float) * nelements;
    CUDA_SAFE_CALL( cudaMalloc((void**)&array_d, size) );
    CUDA_SAFE_CALL( cudaMemcpy(array_d, array, size, cudaMemcpyHostToDevice) );
    
    test_reduction<<<nblocks, threads_per_block>>>(array_d, nblocks);
    
    CUDA_SAFE_CALL( cudaMemcpy(array, array_d, size, cudaMemcpyDeviceToHost) );
    CUDA_SAFE_CALL( cudaFree(array_d) );
    CUDA_SAFE_CALL( cudaFree(global_sums_d) );
    
    sum = array[0];

    printf("GPU sum: %.4f\n", sum);
    printf("\n");
    
    return 0;
}
