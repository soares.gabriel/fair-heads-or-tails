/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    May 15, 2011
 * @brief   Tests the Baum-Welch Algorithm
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../main/hmm.h"
#include "../main/hmm_test.h"
#include "../main/hmm_bwa_cublas.h"
 
/* Read in HMM and observation from file and test BWA */ 
int main(int argc, char *argv[]) {

    /* Initialize variables */
    int c;
    Hmm *in_hmm;                /* Initial HMM */
    Obs *in_obs;                /* Observation sequence */
    char *configfile = NULL;
    float log_lik;              /* Output likelihood of FO */
    struct timeval timer;
    
    /* Initialize HMM and observation sequence */
    in_hmm = (Hmm *)malloc(sizeof(Hmm));
    in_obs = (Obs *)malloc(sizeof(Obs));
    
    /* Parse CLI arguments */
    while ((c = getopt(argc, argv, "c:n:")) != -1) {
        switch(c) {
        
            /* Define config file */
            case 'c':
                configfile = optarg;
                break;
            
            /* Define case for unknown argument */
            case '?':
                fprintf(stderr, "illegal options\n");
                exit(EXIT_FAILURE);
                
            /* Define default case */
            default:
                abort();
        }
    }
    
    /* Read HMM paramters and observation sequence from file */
    read_hmm_file(configfile, in_hmm, in_obs, 0);

    /* Print initial HMM paramters as extracted from the config file */
    printf("\n");
    printf("HMM parameters read from file:\n");
    print_hmm(in_hmm);
    
    /* Run the BWA on the HMM and observation sequence */
    tic(&timer);
    log_lik = run_hmm_bwa(in_hmm, in_obs, 20, 0);
    toc(&timer);
    printf("Log likelihood: %0.4f\n", log_lik);
    
    /* Print new HMM parameters */
    printf("\n");
    printf("Re-estimated HMM parameters:\n");
    print_hmm(in_hmm);
    
    /* Free memory */
    free_vars(in_hmm, in_obs);
    
    return 0;
}

