/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    June 2, 2011
 * @brief   Time the FO by varying parameters
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include "../main/hmm.h"
#include "../main/hmm_test.h"
#include "../main/hmm_fo_cublas.h"

#define N_TEST      1           /* Run test that varies states */
#define S_TEST      1           /* Run test that varies symbols */
#define T_TEST      1           /* Run test that varies number of observations*/

#define T           1000        /* Number of static observations */
#define S           2           /* Number of static symbols */
#define N           60          /* Number of static states */
#define ITERATIONS  1           /* Number of iterations */

/* Time the forward algorithm and vary the number of states */ 
int main(int argc, char *argv[]) {

    /* Initialize variables */
    Hmm *hmm;                /* Initial HMM */
    Obs *obs;                /* Observation sequence */
    float *a;
    float *b;
    float *pi;
    int *obs_seq;
    float log_lik;           /* Output likelihood of FO */
    int mul;
    int m;
    int n;
    int i;
    struct timeval timer;
    
    printf("\n");
    
#if N_TEST
    
    /* Initialize HMM and observation sequence */
    hmm = (Hmm *)malloc(sizeof(Hmm));
    obs = (Obs *)malloc(sizeof(Obs));
    
    /* Create observation sequence */
    obs->length = T;
    obs_seq = (int *)malloc(sizeof(int) * T);
    for (i = 0; i < T; i++) {
        obs_seq[i] = 0;
    }
    obs->data = obs_seq;
    
    printf("Vary states with S = %i, T = %i\n", S, T);
    printf("States, Time (s)\n");
    
    /* Run timed tests from 1*mul to 9*mul states */
    for (m = 0; m <= 3; m++) {
        mul = 1 * pow(10,m);
        for (n = 1*mul; n < 10*mul; n += mul) {
        
            if (n >= 8000) {
                break;
            }
            
            /* Assign HMM parameters */
            hmm->nstates = n;
            hmm->nsymbols = S;
            a = (float *)malloc(sizeof(float) * n * n);
            for (i = 0; i < (n * n); i++) {
                a[i] = 1.0f/(float)n;
            }
            hmm->a = a;
            b = (float *)malloc(sizeof(float) * n * S);
            for (i = 0; i < (n * S); i++) {
                b[i] = 1.0f/(float)S;
            }
            hmm->b = b;
            pi = (float *)malloc(sizeof(float) * n);
            for (i = 0; i < n; i++) {
                pi[i] = 1.0f/(float)n;
            }
            hmm->pi = pi;
            
            /* Run the BWA on the observation sequence */
            printf("%i,", n);
            tic(&timer);
            log_lik = run_hmm_fo(hmm, obs);
            toc(&timer);
            
            /* Free memory */
            free(a);
            free(b);
            free(pi);
        }
    }
    
    hmm->a = NULL;
    hmm->b = NULL;
    hmm->pi = NULL;
    free_vars(hmm, obs);
    
    printf("\n");
    
#endif

#if S_TEST
    
    /* Initialize HMM and observation sequence */
    hmm = (Hmm *)malloc(sizeof(Hmm));
    obs = (Obs *)malloc(sizeof(Obs));
    
    /* Create observation sequence */
    obs->length = T;
    obs_seq = (int *)malloc(sizeof(int) * T);
    for (i = 0; i < T; i++) {
        obs_seq[i] = 0;
    }
    obs->data = obs_seq;
    
    printf("Vary symbols with N = %i, T = %i\n", N, T);
    printf("Symbols, Time (s)\n");
    
    /* Run timed tests from 1*mul to 9*mul symbols */
    for (m = 0; m <= 3; m++) {
        mul = 1 * pow(10,m);
        for (n = 1*mul; n < 10*mul; n += mul) {
        
            if (n >= 8000) {
                break;
            }
            
            /* Assign HMM parameters */
            hmm->nstates = N;
            hmm->nsymbols = n;
            a = (float *)malloc(sizeof(float) * N * N);
            for (i = 0; i < (N * N); i++) {
                a[i] = 1.0f/(float)N;
            }
            hmm->a = a;
            b = (float *)malloc(sizeof(float) * N * n);
            for (i = 0; i < (N * n); i++) {
                b[i] = 1.0f/(float)n;
            }
            hmm->b = b;
            pi = (float *)malloc(sizeof(float) * N);
            for (i = 0; i < N; i++) {
                pi[i] = 1.0f/(float)N;
            }
            hmm->pi = pi;
            
            /* Run the BWA on the observation sequence */
            printf("%i,", n);
            tic(&timer);
            log_lik = run_hmm_fo(hmm, obs);
            toc(&timer);
            
            /* Free memory */
            free(a);
            free(b);
            free(pi);
        }
    }
    
    hmm->a = NULL;
    hmm->b = NULL;
    hmm->pi = NULL;
    free_vars(hmm, obs);
    
    printf("\n");
    
#endif

#if T_TEST

    /* Initialize HMM and observation sequence */
    hmm = (Hmm *)malloc(sizeof(Hmm));
    obs = (Obs *)malloc(sizeof(Obs));
    
    /* Create HMM */
    hmm->nstates = N;
    hmm->nsymbols = S;
    a = (float *)malloc(sizeof(float) * N * N);
    for (i = 0; i < (N * N); i++) {
        a[i] = 1.0f/(float)N;
    }
    hmm->a = a;
    b = (float *)malloc(sizeof(float) * N * S);
    for (i = 0; i < (N * S); i++) {
        b[i] = 1.0f/(float)S;
    }
    hmm->b = b;
    pi = (float *)malloc(sizeof(float) * N);
    for (i = 0; i < N; i++) {
        pi[i] = 1.0f/(float)N;
    }
    hmm->pi = pi;
    
    printf("Vary observations with N = %i, S = %i\n", N, S);
    printf("Observations, Time (s)\n");
    
    /* Run timed tests varying observations */
    for (n = 10; n <= 90; n+=10) {
        
        /* Create observation sequence */
        obs->length = n;
        obs_seq = (int *)malloc(sizeof(int) * n);
        for (i = 0; i < n; i++) {
            obs_seq[i] = 0;
        }
        obs->data = obs_seq;
        
        /* Run the BWA on the observation sequence */
        printf("%i,", n);
        tic(&timer);
        log_lik = run_hmm_fo(hmm, obs);
        toc(&timer);
        
        /* Free memory */
        free(obs_seq);
        
    }
    for (n = 100; n <= 900; n+=100) {
        
        /* Create observation sequence */
        obs->length = n;
        obs_seq = (int *)malloc(sizeof(int) * n);
        for (i = 0; i < n; i++) {
            obs_seq[i] = 0;
        }
        obs->data = obs_seq;
        
        /* Run the BWA on the observation sequence */
        printf("%i,", n);
        tic(&timer);
        log_lik = run_hmm_fo(hmm, obs);
        toc(&timer);
        
        /* Free memory */
        free(obs_seq);
        
    }
    for (n = 1000; n <= 9000; n+=1000) {
        
        /* Create observation sequence */
        obs->length = n;
        obs_seq = (int *)malloc(sizeof(int) * n);
        for (i = 0; i < n; i++) {
            obs_seq[i] = 0;
        }
        obs->data = obs_seq;
        
        /* Run the BWA on the observation sequence */
        printf("%i,", n);
        tic(&timer);
        log_lik = run_hmm_fo(hmm, obs);
        toc(&timer);
        
        /* Free memory */
        free(obs_seq);
        
    }
    
    obs->data = NULL;
    free_vars(hmm, obs);
    
    printf("\n");
    
#endif
    
    return 0;
}
