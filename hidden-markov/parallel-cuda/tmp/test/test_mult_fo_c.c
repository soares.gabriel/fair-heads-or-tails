/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    October 24, 2011
 * @brief   Tests the forward algorithm
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../main/hmm.h"
#include "../main/hmm_test.h"
#include "../main/hmms_fo_c.h"
 
/* Read in HMM and observation from file and test FO */ 
int main(int argc, char *argv[]) {

    /* Initialize variables */
    int c;
    int h;
    int nstates;
    int nsymbols;
    int nmodels = 5;
    Hmm *in_hmm[nmodels];                /* Initial HMM */
    Obs *in_obs;                /* Observation sequence */
    float log_liks[nmodels];            /* Log likelihoods */
    struct timeval timer;
    char *configfiles[] = { "resources/scf_images/models/hmm_2fsk.txt",
                            "resources/scf_images/models/hmm_4fsk.txt",
                            "resources/scf_images/models/hmm_baseband.txt",
                            "resources/scf_images/models/hmm_bpsk.txt",
                            "resources/scf_images/models/hmm_qpsk.txt"};
    
    /* Initialize HMM and observation sequence */
    in_obs = (Obs *)malloc(sizeof(Obs));
    for (h = 0; h < nmodels; h++) {
        in_hmm[h] = (Hmm *)malloc(sizeof(Hmm) * nmodels);
    }
    
    /* Read HMM paramters and observation sequence from file */
    for (h = 0; h < nmodels; h++) {
        read_hmm_file(configfiles[h], in_hmm[h], in_obs, 1);
    }

    nstates = in_hmm[0]->nstates;
    nsymbols = in_hmm[0]->nsymbols;
    printf("NSTATES: %i\n", nstates);
    printf("NSYMBOLS: %i\n", nsymbols);
    printf("T: %i\n",in_obs->length);
    
    /* Run the forward algorithm on the observation sequence */
    tic(&timer);
    run_hmms_fo(in_hmm, in_obs, nstates, nsymbols, nmodels, log_liks);
    toc(&timer);
    
    /* Display log likelihoods */
    printf("Log likelihoods:\n");
    for (h = 0; h < nmodels; h++) {
        printf("Log likelihood: %0.4f\n", log_liks[h]);
    }
    
    /* Free memory */
    free(in_obs);
    for (h = 0; h < nmodels; h++) {
        free(in_hmm[h]);
    }
    
    return 0;
}

