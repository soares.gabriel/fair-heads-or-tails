/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    October 25, 2011
 * @brief   Time the multiple-model FO by varying parameters using CUDA
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include "../main/hmm.h"
#include "../main/hmm_test.h"
#include "../main/hmms_fo_cuda.h"

#define H_TEST      1           /* Run test that varies models */
#define N_TEST      1           /* Run test that varies states */
#define S_TEST      1           /* Run test that varies symbols */
#define T_TEST      1           /* Run test that varies number of observations*/

#define T           6790        /* Number of static observations */
#define M           256         /* Number of static symbols */
#define N           2           /* Number of static states */
#define H           5           /* Number of static models */
#define MAX_MODELS  1000        /* Maximum number of models to use */
#define ITERATIONS  1           /* Number of iterations */

/* Time the forward algorithm and vary the number of states */ 
int main(int argc, char *argv[]) {

    /* Initialize variables */
    Hmm *hmms[MAX_MODELS];      /* Initial HMMs */
    Obs *obs;                   /* Observation sequence */
    float *a;
    float *b;
    float *pi;
    int *obs_seq;
    float log_liks[MAX_MODELS]; /* Output likelihood of FO */
    int mul;
    int m;
    int n;
    int i;
    int h;
    struct timeval timer;
    
    printf("\n");
    
#if H_TEST

    /* Initialize HMM and observation sequence */
    obs = (Obs *)malloc(sizeof(Obs));
    for (h = 0; h < MAX_MODELS; h++) {
        hmms[h] = (Hmm *)malloc(sizeof(Hmm) * MAX_MODELS);
    }
    
    /* Create observation sequence */
    obs->length = T;
    obs_seq = (int *)malloc(sizeof(int) * T);
    for (i = 0; i < T; i++) {
        obs_seq[i] = 0;
    }
    obs->data = obs_seq;
    
    printf("Vary models with N = %i, M = %i, T = %i\n", N, M, T);
    printf("States, Time (s)\n");
    
    /* Create HMMs */
    a = (float *)malloc(sizeof(float) * N * N);
    for (i = 0; i < (N * N); i++) {
        a[i] = 1.0f/(float)N;
    }
    b = (float *)malloc(sizeof(float) * N * M);
    for (i = 0; i < (N * M); i++) {
        b[i] = 1.0f/(float)M;
    }
    pi = (float *)malloc(sizeof(float) * N);
    for (i = 0; i < N; i++) {
        pi[i] = 1.0f/(float)N;
    }
    for (h = 0; h < MAX_MODELS; h++) {
        hmms[h]->nstates = N;
        hmms[h]->nstates = M;
        hmms[h]->a = a;
        hmms[h]->b = b;
        hmms[h]->pi = pi;
    }
    
    /* Run Forward Algorithm on each model, increasing number of models */
    for (h = 1; h <= MAX_MODELS; h++) {
        printf("%i,", h);
        tic(&timer);
        run_hmms_fo(hmms, obs, N, M, h, log_liks);
        toc(&timer);
    }
        
    /* Free memory */    
    free(a);
    free(b);
    free(pi);
    free(obs);
    for (h = 0; h < MAX_MODELS; h++) {
        free(hmms[h]);
    }
    
#endif
    
}
