/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    February 23, 2011
 * @brief   Defines several important structures and functions for HMMs
 */
 
#ifndef HMM_H
#define HMM_H

#ifndef TRUE
#define TRUE    1           /**< Boolean TRUE */
#endif /* TRUE */
#ifndef FALSE
#define FALSE   0           /**< Boolean FALSE */
#endif /* FALSE */
 

 /**
  * @brief contins the individual paramters of a Hidden Markov Model
  */
typedef struct {
    int nstates;            /**< number of states in the HMM */
    int nsymbols;           /**< number of possible symbols */
    float *a;               /**< A matrix - state transition probabilities */
    float *b;               /**< B matrix - symbol output probabilities */
    float *pi;              /**< Pi matrix - initial state probabilities */
} Hmm;


/**
 * @brief contains the observation sequence and length
 */
typedef struct {
    int length;             /**< the length of the observation sequence */
    int *data;              /**< the observation sequence */
} Obs;


/**
 * @brief set the state transition matrix (A)
 *
 * @param[in] a A matrix - state transistion probabilities
 */
void set_hmm_a(float *a);


/**
 * @brief set the output probability matrix (B)
 *
 * @param[in] b B matrix - output probabilities
 */
void set_hmm_b(float *b);


/**
 * @brief set the initial probability matrix
 *
 * @param[in] pi Pi matrix - initial probabilities
 */
void set_hmm_pi(float *pi);

/**
 * @brief print the HMM parameters (A, B, Pi) to the console
 *
 * This function prints the parameters of the Hidden Markov Model to the
 * console. This includes the A, B, and Pi matrices.
 *
 * @param hmm a reference to the Hidden Markov Model
 */
void print_hmm(Hmm *hmm);


/**
 * @brief print the observation sequence to the console
 *
 * @param obs a reference to the observation sequence
 */
void print_obs(Obs *obs);


/**
 * @brief free memory for the HMM parameters and observation sequence
 *
 * This function frees the memory associated with the HMM parameters (A, B,
 * Pi) and the observation sequence.
 *
 * @param[in] hmm the HMM structure to free
 * @param[in] obs the Obs structure to free
 */
void free_vars(Hmm *hmm, Obs *obs);


#endif /* HMM_H */
