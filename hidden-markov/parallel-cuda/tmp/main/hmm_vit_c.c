/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    June 1, 2011
 * @brief   Implements the Viterbi Algorithm in C
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "hmm.h"
#include "hmm_vit_c.h"

/* Runs the Viterbi Algorithm on the supplied HMM and obs. seq. */
void run_hmm_vit(Hmm *hmm, Obs *in_obs, int *state_seq)
{
    float *a = hmm->a;
    float *b = hmm->b;
    float *pi = hmm->pi;
    int nstates = hmm->nstates;
    int nsymbols = hmm->nsymbols;
    int *obs = in_obs->data;
    int length = in_obs->length;
    float *delta;
    int *psi;
    float scale;
    float *inner_product;
    int i;
    int j;
    int t;
    
    delta = (float *) malloc(sizeof(float) * nstates * length);
    psi = (int *) malloc(sizeof(int) * nstates * length);
    inner_product = (float *)malloc(sizeof(float) * nstates);
    
    /* Initialization */
    scale = 0;
    for (i = 0; i < nstates; i++) {
        delta[i] = pi[i] * b[(obs[0] * nstates) + i];
        scale += delta[i];
    }
    for (i = 0; i < nstates; i++) {
        delta[i] /= scale;
    }
    for (i = 0; i < nstates; i++) {
        psi[i] = 0;
    }
    
    /* Recursive step */
    for (t = 1; t < length; t++) {
        scale = 0;
        for (j = 0; j < nstates; j++) {
            for (i = 0; i < nstates; i++) {
                inner_product[i] = delta[((t-1)*nstates) + i] * 
                                        a[(i * nstates) + j];
            }
            delta[(t * nstates) + j] = find_max_val(inner_product, nstates) *
                                        b[(obs[t] * nstates) + j];
            psi[(t * nstates) + j] = find_max_ind(inner_product, nstates);
            scale += delta[(t * nstates) + j];
        }
        for (j = 0; j < nstates; j++) {
            delta[(t * nstates) + j] /= scale;
        }
    }
    
    /* Termination */
    state_seq[length - 1] = find_max_ind(delta + ((length-1) * nstates),
                                                                    nstates);
    
    /* Find most probable path */
    for (t = length - 2; t >= 0; t--) {
        state_seq[t] = psi[((t+1) * nstates) + state_seq[t+1]];
    }
    
    free(delta);
    free(psi);
    free(inner_product);
}

/* Find the maximum value of an element in an array */
float find_max_val(float *array, int length) 
{
    
    float max;
    int i;
    
    max = array[0];
    for (i = 0; i < length; i++) {
        if (array[i] > max) {
            max = array[i];
        }
    }
    
    return max;
}

/* Find the index of the maximum value in an array */
int find_max_ind(float *array, int length) 
{
    
    float max;
    int ind;
    int i;
    
    max = array[0];
    ind = 0;
    for (i = 0; i < length; i++) {
        if (array[i] > max) {
            max = array[i];
            ind = i;
        }
    }
    
    return ind;
}

