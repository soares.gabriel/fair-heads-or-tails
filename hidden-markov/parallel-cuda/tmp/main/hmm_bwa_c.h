/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    May 15, 2011
 * @brief   Defines C code necessary for the Baum-Welch Algorithm
 *
 * The Baum-Welch Algorithm (BWA) re-estimates HMM parameters using the supplied
 * observation sequence.
 *
 * To use the BWA, pass the initial HMM, observation sequence, number of
 * iterations of the BWA, threshold of log likelihood between iterations, and
 * a container for the output HMM.
 */
 
 #ifndef HMM_BWA_C_H
 #define HMM_BWA_C_H
 
 /**
  * @brief run the Baum-Wlech Algorithm on the supplied HMM and observation
  *
  * @param[in,out] hmm the HMM to be re-estimated
  * @param[in] in_obs observation sequence
  * @param[in] iterations maximum number of times to run the BWA
  * @param[in] threshold minimum log likelihood difference between BWA runs
  * @param[out] out_hmm re-estimated Hidden Markov Model
  * @return log10 of the probability that the HMM matches the sequence
  */
float run_hmm_bwa(  Hmm *hmm, 
                    Obs *in_obs, 
                    int iterations, 
                    float threshold);
                    
/**
 * @brief calculates the forward variables (alpha) of an HMM with sequence
 *
 * @param[in] in_hmm the HMM to use for forward variable calculation
 * @param[in] in_obs the observation sequence used to calculate alpha
 * @param[out] alpha the forward variables
 * @param[out] scale scaling factor array for alpha variables
 * @return log10 of the probability that the HMM matches the sequence
 */
float calc_alpha(Hmm *in_hmm, Obs *in_obs, float *alpha, float *scale);

/**
 * @brief calculates the backward variables (beta) of an HMM with sequence
 *
 * @param[in] in_hmm the HMM to use for backward variable calculation
 * @param[in] in_obs the observation sequence used to calculate beta
 * @param[in] scale the scaling factor array for alpha variables
 * @param[out] beta the backward variables
 */
void calc_beta(Hmm *in_hmm, Obs *in_obs, float *scale, float *beta);

/**
 * @brief calculates the sum of the gamma variables
 *
 * @param[in] alpha the forward variables
 * @param[in] beta the backward variables
 * @param[in] nstates the number of states in the HMM
 * @param[in] length the length of the observation sequence
 * @param[out] gamma_sum the sum of the gamma variables
 */
void calc_gamma_sum(    float *alpha, 
                        float *beta, 
                        int nstates, 
                        int length, 
                        float *gamma_sum);
                        
/**
 * @brief calculates the sum of the xi variables
 *
 * @param[in] in_hmm the HMM to use
 * @param[in] in_obs the observation sequence used to calculate xi
 * @param[in] alpha the forward variables
 * @param[in] beta the backward variables
 * @param[out] gamma_sum the sum of the gamma variables
 */
void calc_xi_sum(   Hmm *in_hmm, 
                    Obs *in_obs, 
                    float *alpha, 
                    float *beta, 
                    float *xi_sum);
                    
/**
 * @brief re-estimates the state transistion probabilities (A)
 *
 * @param[in] alpha the forward variables
 * @param[in] beta the backward variables
 * @param[in] nstates the number of states in the HMM
 * @param[in] length the number of observations
 * @param[in] gamma_sum the sum of the gamma variables
 * @param[in] xi_sum the sum of the xi variables
 * @param[out] a the state transistion probabilities
 */              
void estimate_a(    float *alpha, 
                    float *beta, 
                    int nstates,
                    int length,
                    float *gamma_sum, 
                    float *xi_sum, 
                    float *a);
                    
/**
 * @brief re-estimates the symbol output probabilities (B)
 *
 * @param[in,out] hmm the hmm to re-estimate
 * @param[in] in_obs the observation sequence used to train the hmm
 * @param[in] alpha the forward variables
 * @param[in] beta the backward variables
 * @param[in] gamma_sum the sum of the gamma variables
 */
void estimate_b(    Hmm *hmm, 
                    Obs *in_obs, 
                    float *alpha, 
                    float *beta, 
                    float *gamma_sum);
                    
/**
 * @brief re-estimates the initial state probabilities (Pi)
 *
 * @param[in] alpha the forward variables
 * @param[in] beta the backward variables
 * @param[in] nstates the number of states in the HMM
 * @param[out] pi the initial state probabilities
 */
void estimate_pi(float *alpha, float *beta, int nstates, float *pi);

#endif /* HMM_BWA_C_H */
