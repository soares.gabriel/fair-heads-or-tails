/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    March 25, 2011
 * @brief   Defines C code necessary for the forward algorithm
 *
 * The Forward Algorithm calculates the probability that the observation
 * sequence matches a given Hidden Markov Model (HMM).
 *
 * To use the forward algorithm, pass in a HMM and an observation sequence. Note
 * that in order to prevent underflow, the output is given in log10 format.
 * I.e. log_lik = log10(Pr)
 */
 
 #ifndef HMM_FO_C_H
 #define HMM_FO_C_H
 
 /**
  * @brief run the forward algorithm  on the provided HMM and sequence
  *
  * @param[in] in_hmm Hidden Markov Model
  * @param[in] in_obs observation sequence
  * @return log10 of the probability that the HMM matches the sequence
  */
float run_hmm_fo(Hmm *in_hmm, Obs *in_obs);

#endif /* HMM_FO_C_H */
