/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    March 27, 2011
 * @brief   Defines CUDA code necessary for the Forward Algorithm
 *
 * The forward algorithm (FO) calculates the probability of producing the 
 * observation sequence given a Hidden Markov Model (HMM). This particular
 * implementation relies on the CUBLAS library.
 *
 * To use the forward algorithm, supply the code with an observation sequence
 * and HMM parameters.
 *
 * TODO: Optimize the FO routine by removing alpha and scale matrices. We don't
 * need them for calculating the likelihood.
 */
 
#ifndef HMM_FO_CUBLAS_H
#define HMM_FO_CUBLAS_H

/**
 * @brief run the forward algorithm on the provided HMM and sequence
 *
 * @param[in] in_hmm the Hidden Markov Model
 * @param[in] in_obs the observation sequence
 * @return 1.0f if error, log10 of the probability otherwise
 */
float run_hmm_fo(Hmm *in_hmm, Obs *in_obs);

#endif /* HMM_FO_CUBLAS_H */
