/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    March 18, 2011
 * @brief   Defines several important functions for testing HMMs
 */
 
#ifndef HMM_TEST_H
#define HMM_TEST_H

/**
 * @brief read in HMM paramter and observation sequence from a file
 *
 * @param[in] configfile the location of the config file
 * @param[out] hmm the HMM paramters
 * @param[out] obs the observation sequence
 * @param[in] trans 1 if the B matrix is transposed (across is states)
 */
void read_hmm_file(char *configfile, Hmm *hmm, Obs *obs, int transpose);


/**
 * @brief subtracts timeval structs used for timing application run time
 *
 * Credit goes to Amro on stackoverflow.com for this implementation
 *
 * @param[out] result difference between time values
 * @param[in] t2 larger time
 * @param[in] t1 smaller time
 * @return 1 if differnce is negative, 0 otherwise
 */
int timeval_subtract(struct timeval *result, struct timeval *t2, 
                                            struct timeval *t1);
                                            
/**
 * @brief starts timer
 *
 * @param[out] timer timeval structure used to hold the start time
 */
void tic(struct timeval *timer);

/**
 * @brief stops timer and prints result to screen. 
 *
 * @param[in] timer timeval structure containing the start time
 */
void toc(struct timeval *timer);

#endif /* HMM_TEST_H */
