/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
 
/**
 * @file
 * @author  Shawn Hymel
 * @date    October 23, 2011
 * @brief   Implements the Forward Algorithm in C using multiple HMMs
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "hmm.h"

/* Runs the forward algorithm on the supplied HMMs and observation sequence */
void run_hmms_fo(Hmm **in_hmms, Obs *in_obs, int nstates, int nsymbols, 
                                                int nmodels, float log_liks[])
{
    int h;
    int i;
    int j;
    int t;
    float log_lik;
    float *alpha;
    float *scale;
    float *a[nmodels];
    float *b[nmodels];
    float *pi[nmodels];
    int *obs = in_obs->data;
    int length = in_obs->length;
    int size;
    
    alpha = (float *) malloc(sizeof(float) * nstates * length * nmodels);
    if (alpha == NULL) {
        printf("Out of memory! Cannot allocate alpha.\n");
        return;
    }
    scale = (float *) malloc(sizeof(float) * length * nmodels);
    
    /* Create pointers to HMM parameters */
    for (h = 0; h < nmodels; h++) {
        a[h] = in_hmms[h]->a;
        b[h] = in_hmms[h]->b;
        pi[h] = in_hmms[h]->pi;
    }
    
    /* Initialize alpha variables and accumulate scaling factor */
    for (h = 0; h < nmodels; h++) {
        scale[h] = 0;
        for (i = 0; i < nstates; i++) {
            alpha[(h*nstates*length) + i] = pi[h][i] * 
                                            b[h][(obs[0]*nstates) + i];
            scale[h] += alpha[(h*nstates*length) + i];
        }
        for (i = 0; i < nstates; i++) {
            alpha[(h*nstates*length) + i] /= scale[h];
        }
    }
    
    /* Induction */
    for (t = 1; t < length; t++) {
        for (h = 0; h < nmodels; h++) {
            scale[(t*nmodels) + h] = 0;
            for (i = 0; i < nstates; i++) {
                alpha[(h*nstates*length) + (t*nstates) + i] = 0;
                for (j = 0; j < nstates; j++) {
                    alpha[(h*nstates*length) + (t*nstates) + i] += 
                            alpha[(h*nstates*length) + ((t-1)*nstates) + j] * 
                            a[h][(j*nstates) + i] * b[h][(obs[t]*nstates) + i];
                }
                scale[(t*nmodels) + h] += 
                                alpha[(h*nstates*length) + (t*nstates) + i];
            }
            for (i = 0; i < nstates; i++) {
                alpha[(h*nstates*length) + (t*nstates) + i] /= scale[(t*nmodels) + h];
            }
        }
    }

    /* Termination */
    for (h = 0; h < nmodels; h++) {
        log_liks[h] = 0;
        for (t = 0; t < length; t++) {
            log_liks[h] += log10(scale[(t*nmodels) + h]);
        }
    }

    /* Free allocated variables */
    free(alpha);
    free(scale);

}
