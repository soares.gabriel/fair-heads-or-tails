/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    May 28, 2011
 * @brief   Defines CUDA code necessary for the Baum-Welch Algorithm
 *
 * The Baum-Welch Algorithm re-estimates the parameters of an HMM using a 
 * supplied observation sequence. This particular implementation relies on the
 * CUBLAS library.
 */
 
#ifndef HMM_BWA_CUBLAS_H
#define HMM_BWA_CUBLAS_H

 /**
  * @brief run the Baum-Wlech Algorithm on the supplied HMM and observation
  *
  * @param[in,out] hmm the HMM to be re-estimated
  * @param[in] in_obs observation sequence
  * @param[in] iterations maximum number of times to run the BWA
  * @param[in] threshold minimum log likelihood difference between BWA runs
  * @param[out] out_hmm re-estimated Hidden Markov Model
  * @return log10 of the probability that the HMM matches the sequence
  */
float run_hmm_bwa(  Hmm *hmm, 
                    Obs *in_obs, 
                    int iterations, 
                    float threshold);
               
/**
 * @brief calculates the forward variables (alpha) of an HMM with sequence
 *
 * @return log10 of the probability that the HMM matches the sequence
 */    
float calc_alpha();

/**
 * @brief calculates the backward variables (beta)
 */
int calc_beta();

/**
 * @brief calculates the gamma sum
 */
void calc_gamma_sum();

/**
 * @brief calculates the sum of the xi variables
 */
int calc_xi_sum();

/**
 * @brief re-estimates the state transition probabilities (A)
 */
int estimate_a();

/**
 * @brief re-estimates the symbol output probabilities (B)
 */
int estimate_b();

/**
 * @brief re-estimates the initial state probabilities (Pi)
 */
int estimate_pi();

#endif /* HMM_BWA_CUBLAS_H */
