/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    March 25, 2011
 * @brief   Implements the Forward Algorithm in C
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "hmm.h"
 
/* Runs the forward algorithm on the supplied HMM and observation sequence */
float run_hmm_fo(Hmm *in_hmm, Obs *in_obs)
{
    int j;
    int k;
    int t;
    float log_lik;
    float *alpha;
    float *scale;
    float *a = in_hmm->a;
    float *b = in_hmm->b;
    float *pi = in_hmm->pi;
    int nstates = in_hmm->nstates;
    int *obs = in_obs->data;
    int length = in_obs->length;
    
    alpha = (float *) malloc(sizeof(float) * nstates * length);
    scale = (float *) malloc(sizeof(float) * length);
    
    /* Initialize alpha variables and accumulate scaling factor */
    scale[0] = 0;
    for (j = 0; j < nstates; j++) {
        alpha[j] = pi[j] * b[(obs[0] * nstates) + j];
        scale[0] += alpha[j];
    }
    
    /* Scale initial variables */
    for (j = 0; j < nstates; j++) {
        alpha[j] /= scale[0];
    }
    
    /* Induction step - calculate alpha variables and apply scaling factor */
    for (t = 1; t < length; t++) {
        scale[t] = 0;
        for (j = 0; j < nstates; j++) {
            alpha[(t * nstates) + j] = 0;
            for (k = 0; k < nstates; k++) {
                alpha[(t * nstates) + j] += alpha[((t-1) * nstates) + k] * 
                                            a[(k * nstates) + j] *
                                            b[(obs[t] * nstates) + j];
            }
            scale[t] += alpha[(t * nstates) + j];
        }
        for (j = 0; j < nstates; j++) {
            alpha[(t * nstates) + j] /= scale[t];
        }
    }
    
    /* Calculate the log10(likelihood) */
    log_lik = 0;
    for (t = 0; t < length; t++) {
        log_lik += log10(scale[t]);
    }
    
    free(alpha);
    free(scale);

    return log_lik;
}
            
