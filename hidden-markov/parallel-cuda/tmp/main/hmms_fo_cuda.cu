/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    October 24, 2011
 * @brief   Implements the Forward Algorithm (FO) in CUDA
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <cuda.h>
#include <cutil.h>
 
#include "hmm.h"
extern "C" {                    /* required for functions to be visible in C */
#include "hmms_fo_cuda.h"
}

/* Contains information about the graphics card's CUDA capabilities */
enum {
    MAX_STATES = 16             /* Static depending on gfx card capabilities*/
};

/******************************************************************************/
/* CUDA - Run Forward Algorithm on multiple models */
__global__ void calc_alpha_dev( float *a_d,
                                float *b_d,
                                float *pi_d,
                                int nstates,
                                int nsymbols,
                                int length,
                                int nmodels,
                                int *obs_d,
                                float *alpha_d,
                                float *scale_d,
                                float *log_liks_d)
{
 
    unsigned int tdx = threadIdx.x;
    unsigned int tdy = threadIdx.y;
    unsigned int tid = (tdy * blockDim.x) + tdx;
    unsigned int bid = blockIdx.x;
    unsigned int stride;
    int obs_t;
    int t;
    __shared__ float partial_sum[MAX_STATES*MAX_STATES];
    
    /* Initialize alpha */
    if ((tid < nstates) && (bid < nmodels)) {
        obs_t = obs_d[0];
        alpha_d[(bid*nstates*length) + tid] = pi_d[(bid*nstates) + tid] * 
                        b_d[(bid*nstates*nsymbols) + (obs_t*nstates) + tid];
    }
    
    /* Calculate scaling factor and initialize log likelihoods */
    if ((tid < nstates) && (bid < nmodels)) {
        partial_sum[tid] = alpha_d[(bid*nstates*length) + tid];
    } else {
        partial_sum[tid] = 0;
    }
    __syncthreads();
    for (stride = blockDim.x/2; stride > 0; stride /= 2) {
        if ((tid < stride) && (bid < nmodels)) {
            partial_sum[tid] += partial_sum[tid + stride];
        }
        __syncthreads();
    }
    if ((tid == 0) && (bid < nmodels)) {
        scale_d[bid] = partial_sum[0];
        log_liks_d[bid] = log10(scale_d[bid]);
    }
    
    /* Scale initial alpha */
    if ((tid < nstates) && (bid < nmodels)) {
        alpha_d[(bid*nstates*length) + tid] =
                            alpha_d[(bid*nstates*length) + tid] / scale_d[bid];
    }
    
    /* Induction step */
    for (t = 1; t < length; t++) {
    
        obs_t = obs_d[t];
        
        /* Calculate intermediate step in matrix multiplication */
        if ((tdx < nstates) && (tdy < nstates) && (bid < nmodels)) {
            partial_sum[tid] = 
                    alpha_d[(bid*nstates*length) + ((t-1)*nstates) + tdy] *
                    a_d[(bid*nstates*nstates) + (tdy*nstates) + tdx] *
                    b_d[(bid*nstates*nsymbols) + (obs_t*nstates) + tdx];
        } else {
            partial_sum[tid] = 0;
        }
        
        /* Sum up the columns to complete matrix multiplication */
        __syncthreads();
        for (stride = blockDim.y/2; stride > 0; stride /= 2) {
            if ((tdy < stride) && (bid < nmodels)) {
                partial_sum[tid] = partial_sum[tid] + 
                                    partial_sum[tid + (stride*blockDim.x)];
            }
            __syncthreads();
        }
        
        /* Copy to alpha */
        if ((tid < nstates) && (bid < nmodels)) {
            alpha_d[(bid*nstates*length) + (t*nstates) + tid] = 
                                                        partial_sum[tid];
        }
        
        /* Calculate scaling factor */
        if ((tid < nstates) && (bid < nmodels)) {
            partial_sum[tid] = alpha_d[(bid*nstates*length) + 
                                                    (t*nstates) + tid];
        } else {
            partial_sum[tid] = 0;
        }
        __syncthreads();
        for (stride = blockDim.x/2; stride > 0; stride /= 2) {
            if ((tid < stride) && (bid < nmodels)) {
                partial_sum[tid] += partial_sum[tid + stride];
            }
            __syncthreads();
        }
        if ((tid == 0) && (bid < nmodels)) {
            scale_d[(t*nmodels) + bid] = partial_sum[0];
            log_liks_d[bid] += log10(scale_d[(t*nmodels) + bid]);
        }
        
        /* Scale alpha values */
        if ((tid < nstates) && (bid < nmodels)) {
            alpha_d[(bid*nstates*length) + (t*nstates) + tid] = 
                    alpha_d[(bid*nstates*length) + (t*nstates) + tid] / 
                    scale_d[(t*nmodels) + bid];
        }
    }
}


/******************************************************************************/
/* Runs the forward algorithm on the supplied HMM and observation sequence */
void run_hmms_fo(Hmm **in_hmm, Obs *in_obs, int nstates, int nsymbols, 
                                                int nmodels, float log_liks[])
{

    /* Host-side variables */
    int size;
    int h;
    float *alpha;
    int *obs = in_obs->data;
    int length = in_obs->length;
    
    /* Device-side variables */
    float *a_d;
    float *b_d;
    float *pi_d;
    int *obs_d;
    float *alpha_d;
    float *scale_d;
    float *log_liks_d;
    
    alpha = (float *) malloc(sizeof(float) * nstates * length * nmodels);
    
    /* Check for max number of states */
    if (nstates > MAX_STATES) {
        printf("Maximum number of states exceeded.\n");
        return;
    }
    
    /* Allocate memory and copy needed parameters to device */
    size = sizeof(float) * nstates * nstates * nmodels;
    CUDA_SAFE_CALL( cudaMalloc((void**)&a_d, size) );
    size = sizeof(float) * nstates * nstates;
    for (h = 0; h < nmodels; h++) {
        CUDA_SAFE_CALL( cudaMemcpy(     a_d + h*nstates*nstates, 
                                        in_hmm[h]->a, 
                                        size,
                                        cudaMemcpyHostToDevice) );        
    }
    size = sizeof(float) * nstates * nsymbols * nmodels;
    CUDA_SAFE_CALL( cudaMalloc((void**)&b_d, size) );
    size = sizeof(float) * nstates * nsymbols;
    for (h = 0; h < nmodels; h++) {
        CUDA_SAFE_CALL( cudaMemcpy(     b_d + h*nstates*nsymbols, 
                                        in_hmm[h]->b, 
                                        size, 
                                        cudaMemcpyHostToDevice) );
    }
    size = sizeof(float) * nstates * nmodels;
    CUDA_SAFE_CALL( cudaMalloc((void**)&pi_d, size) );
    size = sizeof(float) * nstates;
    for (h = 0; h < nmodels; h++) {
        CUDA_SAFE_CALL( cudaMemcpy(     pi_d + h*nstates, 
                                        in_hmm[h]->pi, 
                                        size, 
                                        cudaMemcpyHostToDevice) );
    }
    size = sizeof(int) * length;
    CUDA_SAFE_CALL( cudaMalloc((void**)&obs_d, size) );
    CUDA_SAFE_CALL( cudaMemcpy(obs_d, obs, size, cudaMemcpyHostToDevice) );
    size = sizeof(float) * nstates * length * nmodels;
    CUDA_SAFE_CALL( cudaMalloc((void**)&alpha_d, size) );
    size = sizeof(float) * length * nmodels;
    CUDA_SAFE_CALL( cudaMalloc((void**)&scale_d, size) );
    size = sizeof(float) * nmodels;
    CUDA_SAFE_CALL( cudaMalloc((void**)&log_liks_d, size) );
    
    
    /* Run Forward Algorithm in CUDA */
    dim3 blocks( nmodels );
    dim3 threadsPerBlock(MAX_STATES, MAX_STATES);
    calc_alpha_dev<<<blocks, threadsPerBlock>>>(    a_d,
                                                    b_d,
                                                    pi_d,
                                                    nstates,
                                                    nsymbols,
                                                    length,
                                                    nmodels,
                                                    obs_d,
                                                    alpha_d,
                                                    scale_d,
                                                    log_liks_d);
    
    /* Copy results from device to host */
    size = sizeof(float) * nmodels;
    CUDA_SAFE_CALL( cudaMemcpy(log_liks, log_liks_d, size, 
                                            cudaMemcpyDeviceToHost) );
    
    /* Free device memory */
    CUDA_SAFE_CALL( cudaFree(a_d) );
    CUDA_SAFE_CALL( cudaFree(b_d) );
    CUDA_SAFE_CALL( cudaFree(pi_d) );
    CUDA_SAFE_CALL( cudaFree(obs_d) );
    CUDA_SAFE_CALL( cudaFree(alpha_d) );
    CUDA_SAFE_CALL( cudaFree(scale_d) );
    CUDA_SAFE_CALL( cudaFree(log_liks_d) );
    
    /* Free allocated variables */
    free(alpha);

}
