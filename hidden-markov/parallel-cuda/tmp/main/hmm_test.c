/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    March 18, 2011
 * @brief   Implements several important functions for testing HMMs
 */

/* Includes */
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include "hmm.h"
#include "hmm_test.h"

/* Macros */
#define IDX(i,j,d) (((i)*(d))+(j))
#define HANDLE_ERROR(msg) \
  do { perror(msg); exit(EXIT_FAILURE); } while (0)
  

/* Subtracts time values to determine run time */
int timeval_subtract(struct timeval *result, struct timeval *t2, 
                                            struct timeval *t1)
{
    long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - 
                    (t1->tv_usec + 1000000 * t1->tv_sec);
    result->tv_sec = diff / 1000000;
    result->tv_usec = diff % 1000000;

    return (diff<0);
}


/* Starts timer */
void tic(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}


/* Stops timer and prints difference to the screen */
void toc(struct timeval *timer)
{
    struct timeval tv_end, tv_diff;
    
    gettimeofday(&tv_end, NULL);
    timeval_subtract(&tv_diff, &tv_end, timer);
    printf("%ld.%06ld\n", tv_diff.tv_sec, tv_diff.tv_usec);
}


/* Reads HMM parameters and observation sequence from a config file */
void read_hmm_file(char *configfile, Hmm *hmm, Obs *obs, int transpose)
{
 
    FILE *fin, *bin;
    
    /* Initialize line buffer */
    char *linebuf = NULL;
    size_t buflen = 0;
    
    /* Initialize parsing variables */
    int c, k;
    float d;
    opterr = 0;
    
    /* Initialize HMM parameters */
    int nStates = 0;            /* Number of possible states */
    int nSymbols = 0;           /* Number of possible symbols */
    int nSeq = 0;               /* Number of observation sequences */
    int length = 0;             /* Length of each observation sequence */
    
    /* Initialize counter variables */
    int i, j;

    /* Open config file */
    if (configfile == NULL) {
        fin = stdin;
    } else {
        fin = fopen(configfile, "r");
        if (fin == NULL) {
            HANDLE_ERROR("fopen");
        }
    }
    
    /* Parse the config file and extract HMM parameters */
    i = 0;
    while ((c = getline(&linebuf, &buflen, fin)) != -1) {
    
        /* Ignore blank and comment lines (start with '#') */
        if (c <= 1 || linebuf[0] == '#') {
            continue;
        }
        
        /* First section - read number of states */
        if (i == 0) {
            if (sscanf(linebuf, "%d", &nStates) != 1) {
                fprintf(stderr, "Config file format error: %d\n", i);
                free_vars(hmm, obs);
                exit(EXIT_FAILURE);
            }
            
            /* Allocate memory for Pi */
            hmm->nstates = nStates;
            hmm->pi = (float *) malloc(sizeof(float) * nStates);
            if (hmm->pi == NULL) {
                HANDLE_ERROR("malloc");
            }
            
            /* Allocate memory for A */
            hmm->a = (float *) malloc(sizeof(float) * nStates * nStates);
            if (hmm->a == NULL) {
                HANDLE_ERROR("malloc");
            }
            
        /* Second section - read number of possible symbols */
        } else if (i == 1) {
            if (sscanf(linebuf, "%d", &nSymbols) != 1) {
                fprintf(stderr, "Config file format error: %d\n", i);
                free_vars(hmm, obs);
                exit(EXIT_FAILURE);
            }
            
            /* Allocate memory for B */
            hmm->nsymbols = nSymbols;
            hmm->b = (float *) malloc(sizeof(float) * nStates * nSymbols);
            if (hmm->b == NULL) {
                HANDLE_ERROR("malloc");
            }
            
        /* Third section - read initial state probabilities (Pi) */
        } else if (i == 2) {
        
            /* Open memory stream and read in line */
            bin = fmemopen(linebuf, buflen, "r");
            if (bin == NULL) {
                HANDLE_ERROR("fmemopen");
            }
            
            /* Parse through memory stream and store probability values */
            for (j = 0; j < nStates; j++) {
                if (fscanf(bin, "%f", &d) != 1) {
                    fprintf(stderr, "Config file format error: %d\n", i);
                    free_vars(hmm, obs);
                    exit(EXIT_FAILURE);
                }
                hmm->pi[j] = d;
            }
            fclose(bin);
            
        /* Fourth section - read in state transition probabilities (A) */
        } else if (i <= 2 + nStates) {
        
            /* Open memory stream and read in line */
            bin = fmemopen(linebuf, buflen, "r");
            if (bin == NULL) {
                HANDLE_ERROR("fmemopen");
            }
            
            /* Parse through memory stream and store probability values */
            for (j = 0; j < nStates; j++) {
                if (fscanf(bin, "%f", &d) != 1) {
                    fprintf(stderr, "Config file format error: %d\n", i);
                    free_vars(hmm, obs);
                    exit(EXIT_FAILURE);
                }
                hmm->a[IDX((i - 3), j, nStates)] = d; 
            }
            fclose(bin);
        
        /* Fifth section - read in output probabilities (B) */
        } else if ((i <= 2 + nStates * 2) && (transpose == 0)) {
        
            /* Open memory stream and read in line */
            bin = fmemopen(linebuf, buflen, "r");
            if (bin == NULL) {
                HANDLE_ERROR("fmemopen");
            }
            
            /* Parse through memory stream and store probability values */
            for (j = 0; j < nSymbols; j++) {
                if (fscanf(bin, "%f", &d) != 1) {
                    fprintf(stderr, "Config file format error: %d\n", i);
                    free_vars(hmm, obs);
                    exit(EXIT_FAILURE);
                }
                hmm->b[j * nStates + (i - 3 - nStates)] = d;
            }
            fclose(bin);
            
        /* Fifth section (again) - read in output probabilities (B) if transposed */
        } else if ((i <= 2 + nStates + nSymbols) && (transpose)) {
        
            /* Open memory stream and read in line */
            bin = fmemopen(linebuf, buflen, "r");
            if (bin == NULL) {
                HANDLE_ERROR("fmemopen");
            }
            
            /* Parse through memory stream and store probability values */
            for (j = 0; j < nStates; j++) {
                if (fscanf(bin, "%f", &d) != 1) {
                    fprintf(stderr, "Config file format error: %d\n", i);
                    free_vars(hmm, obs);
                    exit(EXIT_FAILURE);
                }
                hmm->b[(i - 3 - nStates) * nStates + j] = d;
            }
            fclose(bin);
            
        /* Sixth section - read in data size (seq, length) */
        } else if ((i == 3 + nStates * 2) && (transpose == 0)) {
        
            /* Read in data sequence and length values */
            if (sscanf(linebuf, "%d %d", &nSeq, &length) != 2) {
                fprintf(stderr, "Config file format error: %d\n", i);
                free_vars(hmm, obs);
                exit(EXIT_FAILURE);
            }
            obs->length = nSeq * length;
            
            /* Allocate memory for observation sequence */
            obs->data = (int *) malloc(sizeof(int) * nSeq * length);
            if (obs->data == NULL) {
                HANDLE_ERROR("malloc");
            }
            
        /* Sixth section (again) - read in data size if B is transposed */
        } else if ((i == 3 + nStates + nSymbols) && (transpose)) {
        
            /* Read in data sequence and length values */
            if (sscanf(linebuf, "%d %d", &nSeq, &length) != 2) {
                fprintf(stderr, "Config file format error: %d\n", i);
                free_vars(hmm, obs);
                exit(EXIT_FAILURE);
            }
            obs->length = nSeq * length;
            
            /* Allocate memory for observation sequence */
            obs->data = (int *) malloc(sizeof(int) * nSeq * length);
            if (obs->data == NULL) {
                HANDLE_ERROR("malloc");
            }
            
        /* Seventh section - read in observation sequence */
        } else if ((i <= 3 + nStates * 2 + nSeq) && (transpose == 0)) {
        
            /* Open memory stream and read in line */
            bin = fmemopen(linebuf, buflen, "r");
            if (bin == NULL) {
                HANDLE_ERROR("fmemopen");
            }
            
            /* Parse through observation sequence and store values (read down, then across) */
            for (j = 0; j < length; j++) {
                if (fscanf(bin, "%d", &k) != 1 || k < 0 || k >= nSymbols) {
                    fprintf(stderr, "Config file format error: %d\n", i);
                    free_vars(hmm, obs);
                    exit(EXIT_FAILURE);
                }
                obs->data[j * nSeq + (i - 4 - nStates * 2)] = k;
            }
            fclose(bin);
            
        /* Seventh section (again) - read in observation sequence (if B is transposed)*/
        } else if ((i <= 3 + nStates + nSymbols + nSeq) && (transpose)) {
        
            /* Open memory stream and read in line */
            bin = fmemopen(linebuf, buflen, "r");
            if (bin == NULL) {
                HANDLE_ERROR("fmemopen");
            }
            
            /* Parse through observation sequence and store values (read down, then across) */
            for (j = 0; j < length; j++) {
                if (fscanf(bin, "%d", &k) != 1 || k < 0 || k >= nSymbols) {
                    fprintf(stderr, "Config file format error: %d\n", i);
                    free_vars(hmm, obs);
                    exit(EXIT_FAILURE);
                }
                obs->data[j * nSeq + (i - 4 - (nStates + nSymbols))] = k;
            }
            fclose(bin);
        }
        
        /* Increment line counter */
        i++;
        
    }
    
    /* Close files and free memory */
    fclose(fin);
    if (linebuf) {
        free(linebuf);
    }
    
    /* Throw error if configuration file was not read properly */
    if (i < 4 + nStates * 2 + nSeq) {
        fprintf(stderr, "Configuration incomplete\n");
        free_vars(hmm, obs);
        exit(EXIT_FAILURE);
    }
    
}

