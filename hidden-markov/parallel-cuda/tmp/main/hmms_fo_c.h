/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    October 23, 2011
 * @brief   Defines C code necessary for the forward algorithm and multiple HMMs
 *
 * The Forward Algorithm calculates the probability that the observation
 * sequence matches a given several Hidden Markov Models (HMMs).
 * 
 * To us the forward algorithm, pass in an array of HMMs and an observation
 * sequence and a pointer to an array of log likelihoods (output). Note that in
 * order to prevent underflow, the output is given in log10 format.
 * I.e. log_lik = log10(Pr). IMPORTANT: A, B, and Pi must be the same size for
 * all HMMs!
 */
 
 #ifndef HMMS_FO_C_H
 #define HMMS_FO_C_H
 
 /**
  * @brief run the forward algorithm on the provided HMMs and sequence
  *
  * @param[in] in_hmms pointer to Hidden Markov Models
  * @param[in] in_obs pointer to observation sequence
  * @param[in] num_hmms number of HMMs to compare
  * @param[out] log_liks log10 probabilities that each HMM matches the sequence
  */
void run_hmms_fo(Hmm **in_hmms, Obs *in_obs, int nstates, int nsymbols, 
                                                int nmodels, float log_liks[]);

#endif /* HMMS_FO_C_H */
