/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    June 2, 2011
 * @brief   Defines C code necessary for the Viterbi Algorithm
 *
 * The Viterbi Algorithm calculates the most probable state sequence given
 * an observation sequence.
 *
 * To use the Viterbi, pass in the HMM and an observation sequence as well as
 * a pointer to the output of the state sequence.
 */
 
 #ifndef HMM_VIT_CUBLAS_H
 #define HMM_VIT_CUBLAS_H
 
 /**
  * @brief run the Viterbi Algorithm on the supplied HMM and observation seq
  *
  * @param[in] hmm the HMM to be re-estimated
  * @param[in] in_obs observation sequence
  * @param[out] out_hmm re-estimated Hidden Markov Model
  */
void run_hmm_vit(Hmm *hmm, Obs *in_obs, int *state_seq);

/**
 * @brief find the maximum value in an array
 *
 * @param[in] array the array to find the maximum
 * @param[in] length length of the array
 * @return the maximum value
 */
float find_max_val(float *array, int length);

/**
 * @brief find the index of the maximum value in an array
 *
 * @param[in] array the array to find the maximum index
 * @param[in] length length of the array
 * @return the index of the maximum value
 */
int find_max_ind(float *array, int length);

#endif /* HMM_VIT_CUBLAS_H */
