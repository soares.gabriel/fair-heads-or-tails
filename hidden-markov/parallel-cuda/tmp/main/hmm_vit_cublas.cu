/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    June 1, 2011
 * @brief   Implements the Viterbi Algorithm in CUBLAS
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <cuda.h>
#include <cutil.h>
#include "/usr/local/cuda/include/cublas.h"

#include "hmm.h"
extern "C" {
#include "hmm_vit_cublas.h"
}

/* Contains information about the graphics card's CUDA capabilities */
enum {
    MAX_THREADS_PER_BLOCK = 256,
    BLOCK_DIM = 16
};

/*******************************************************************************
 * Kernels
 */
 
/* Initialize index block */
__global__ void init_idx( int *idx_d, int nstates )
{
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int idy = blockIdx.y * blockDim.y + threadIdx.y;
    
    if (idx < nstates && idy < nstates) {
        idx_d[(idy * nstates) + idx] = idx;
    }
}

/* Initialize delta */
__global__ void init_delta( float *delta_d, 
                            float *pi_d, 
                            float *b_d, 
                            float *ones_n_d,
                            int nstates,
                            int obs_t) 
{
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (idx < nstates) {
        delta_d[idx] = pi_d[idx] * b_d[(obs_t * nstates) + idx];
        ones_n_d[idx] = 1.0f;
    }
}

/* Scale delta values */
__global__ void scale_delta(    float *delta_d, 
                                int nstates, 
                                float sum_delta, 
                                int t )
{
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (idx < nstates) {
        delta_d[(t * nstates) + idx] = delta_d[(t * nstates) + idx] / sum_delta;
    }
}

/* Calculate the inner product for delta and psi */
__global__ void calc_inner_prod( float *inner_prod_d,
                                 float *delta_d, 
                                 float *a_d, 
                                 int nstates,
                                 int t)
{
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int idy = blockIdx.y * blockDim.y + threadIdx.y;
    
    if (idx < nstates && idy < nstates) {
        inner_prod_d[(idy * nstates) + idx] = delta_d[((t-1) * nstates) + idx] *
                                                a_d[(idx * nstates) + idy];
    }
}

/* Perform reduction to find max in each row in an array */
__global__ void max_reduction(  float *idata_d,
                                int *idx_d, 
                                int nstates)
{
    unsigned int stride;
    unsigned int tdx = threadIdx.x;
    unsigned int tdy = threadIdx.y;
    unsigned int bdx = blockIdx.x;
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int idy = blockIdx.y * blockDim.y + threadIdx.y;
    __shared__ float partial_max[BLOCK_DIM * BLOCK_DIM];
    __shared__ float partial_idx[BLOCK_DIM * BLOCK_DIM];
    
    /* Copy data to partial max */
    if (idx < nstates && idy < nstates) {
        partial_max[(tdy * BLOCK_DIM) + tdx] = idata_d[(idy * nstates) + idx];
        partial_idx[(tdy * BLOCK_DIM) + tdx] = idx_d[(idy * nstates) + idx];
        idata_d[(idy * nstates) + idx] = 0;
        idx_d[(idy * nstates) + idx] = -1.0f;
    } else {
        partial_max[(tdy * BLOCK_DIM) + tdx] = 0;
        partial_idx[(tdy * BLOCK_DIM) + tdx] = 0;
    }
    
    /* Calculate max and store in partial max */
    __syncthreads();
    for (stride = blockDim.x/2; stride > 0; stride /= 2) {
        if (tdx < stride && idy < nstates) {
            if (partial_max[(tdy * BLOCK_DIM) + (tdx + stride)] > 
                            partial_max[(tdy * BLOCK_DIM) + tdx]) {
                partial_max[(tdy * BLOCK_DIM) + tdx] = 
                            partial_max[(tdy * BLOCK_DIM) + (tdx + stride)];
                partial_idx[(tdy * BLOCK_DIM) + tdx] = 
                            partial_idx[(tdy * BLOCK_DIM) + (tdx + stride)];
            }
        }
        __syncthreads();
    }
    
    /* Store answer */
    if (tdx == 0 && idy < nstates) {
        idata_d[(idy * nstates) + bdx] = partial_max[tdy * BLOCK_DIM];
        idx_d[(idy * nstates) + bdx] = partial_idx[tdy * BLOCK_DIM];
    }
}

/* Calculate delta */
__global__ void calc_delta_psi( float *delta_d, 
                            int *psi_d,
                            float *inner_prod_d,
                            float *b_d, 
                            int *idx_d,
                            int obs_t, 
                            int nstates,
                            int t)
{
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (idx < nstates) {
        delta_d[(t * nstates) + idx] = inner_prod_d[idx * nstates] * 
                                        b_d[(obs_t * nstates) + idx];
        psi_d[(t * nstates) + idx] = idx_d[idx * nstates];
    }
}
 
 /*******************************************************************************
 * Viterbi Function
 */
 
/* Runs the Viterbi Algorithm on the supplied HMM and observation sequence */
void run_hmm_vit(Hmm *hmm, Obs *in_obs, int *state_seq)
{

    /* Host-side variables */
    float *a = hmm->a;
    float *b = hmm->b;
    float *pi = hmm->pi;
    int nstates = hmm->nstates;
    int nsymbols = hmm->nsymbols;
    int *obs = in_obs->data;
    int *psi;
    int length = in_obs->length;
    int threads_per_block;
    int nblocks;
    int max_len;
    int needed_blocks;
    int size;
    float sum_delta;
    cublasStatus cublas_status;
    int t = 0;
    
    /* Device-side variables */
    float *a_d;
    float *b_d;
    float *pi_d;
    float *delta_d;
    int *psi_d;
    float *inner_prod_d;
    float *ones_n_d;
    int *idx_d;

    /* Initialize CUBLAS */
    cublas_status = cublasInit();
    if (cublas_status != CUBLAS_STATUS_SUCCESS) {
        fprintf (stderr, "ERROR: CUBLAS Initialization failure\n");
        return;
    }
    
    /* Allocate memory */
    psi = (int *)malloc(sizeof(int) * length * nstates);
    
    /* Allocate device memory */
    size = sizeof(float) * nstates * nstates;
    CUDA_SAFE_CALL( cudaMalloc((void**)&a_d, size) );
    CUDA_SAFE_CALL( cudaMemcpy(a_d, a, size, cudaMemcpyHostToDevice) );
    size = sizeof(float) * nstates * nsymbols;
    CUDA_SAFE_CALL( cudaMalloc((void**)&b_d, size) );
    CUDA_SAFE_CALL( cudaMemcpy(b_d, b, size, cudaMemcpyHostToDevice) );
    size = sizeof(float) * nstates;
    CUDA_SAFE_CALL( cudaMalloc((void**)&pi_d, size) );
    CUDA_SAFE_CALL( cudaMemcpy(pi_d, pi, size, cudaMemcpyHostToDevice) );
    size = sizeof(float) * nstates * length;
    CUDA_SAFE_CALL( cudaMalloc((void**)&delta_d, size) );
    size = sizeof(int) * nstates * length;
    CUDA_SAFE_CALL( cudaMalloc((void**)&psi_d, size) );
    size = sizeof(float) * nstates * nstates;
    CUDA_SAFE_CALL( cudaMalloc((void**)&inner_prod_d, size) );
    size = sizeof(float) * nstates;
    CUDA_SAFE_CALL( cudaMalloc((void**)&ones_n_d, size) );
    size = sizeof(float) * nstates * nstates;
    CUDA_SAFE_CALL( cudaMalloc((void**)&idx_d, size) );
    
    /* Initialization */
    threads_per_block = MAX_THREADS_PER_BLOCK;
    nblocks = (nstates + threads_per_block - 1) / threads_per_block;
    init_delta<<<nblocks, threads_per_block>>>( delta_d, 
                                                pi_d, 
                                                b_d, 
                                                ones_n_d, 
                                                nstates, 
                                                obs[0]);
    sum_delta = cublasSdot(nstates, delta_d, 1, ones_n_d, 1);
    cublas_status = cublasGetError();
    if (cublas_status != CUBLAS_STATUS_SUCCESS) {
        fprintf (stderr, "ERROR: Kernel execution error\n");
        return;
    }
    scale_delta<<<nblocks, threads_per_block>>>( delta_d, 
                                                nstates, 
                                                sum_delta, 
                                                0 );
    size = sizeof(float) * nstates;
    CUDA_SAFE_CALL( cudaMemset(psi_d, 0, size) );
    
    /* Recursive step */
    for (t = 1; t < length; t++) {
    
        dim3 threads(BLOCK_DIM, BLOCK_DIM);
        nblocks = (nstates + BLOCK_DIM - 1) / BLOCK_DIM;
        dim3 grid(nblocks, nblocks);
    
        /* Initialize index block */
        init_idx<<<grid, threads>>>(idx_d, nstates);
        
        /* Calculate inner product */
        calc_inner_prod<<<grid, threads>>>( inner_prod_d,
                                            delta_d, 
                                            a_d, 
                                            nstates,
                                            t);
        
        /* Calculate maximum values in each vector */
        size = sizeof(float) * nstates * nstates;
        needed_blocks = (nstates + BLOCK_DIM - 1) / BLOCK_DIM;
        max_len = nstates;
        while (max_len > 1) {
            
            max_reduction<<<grid, threads>>>(   inner_prod_d,
                                                idx_d, 
                                                nstates);
                                                
            max_len = needed_blocks;
            needed_blocks = (max_len + BLOCK_DIM - 1) / BLOCK_DIM;      
        }
       
        /* Calculate delta and psi */
        threads_per_block = MAX_THREADS_PER_BLOCK;
        nblocks = (nstates + threads_per_block - 1) / threads_per_block;
        calc_delta_psi<<<nblocks, threads_per_block>>>( delta_d,
                                                        psi_d,
                                                        inner_prod_d,
                                                        b_d, 
                                                        idx_d,
                                                        obs[t], 
                                                        nstates,
                                                        t);
        sum_delta = cublasSdot(nstates, delta_d + (t*nstates), 1, ones_n_d, 1);
        cublas_status = cublasGetError();
        if (cublas_status != CUBLAS_STATUS_SUCCESS) {
            fprintf (stderr, "ERROR: Kernel execution error\n");
            return;
        }
        scale_delta<<<nblocks, threads_per_block>>>(    delta_d, 
                                                        nstates, 
                                                        sum_delta,
                                                        t);

    }
    
    /* Termination */
    state_seq[length - 1] = cublasIsamax(   nstates, 
                                            delta_d + ((length-1) * nstates),
                                            1);
    state_seq[length - 1] = state_seq[length - 1] - 1; /* Fix 1-based index */
                                            
    /* Find most probable path */
    size = sizeof(int) * nstates * length;
    CUDA_SAFE_CALL( cudaMemcpy(psi, psi_d, size, cudaMemcpyDeviceToHost) );
    for (t = length - 2; t >= 0; t--) {
        state_seq[t] = psi[((t+1) * nstates) + state_seq[t+1]];
    }
    
    /* Shutdown CUBLAS */
    if(cublasShutdown() != CUBLAS_STATUS_SUCCESS) {
        printf("ERROR: CUBLAS Shutdown failure\n");
        return;
    }
    
    /* Free memory */
    free(psi);
    CUDA_SAFE_CALL( cudaFree(a_d) );
    CUDA_SAFE_CALL( cudaFree(b_d) );
    CUDA_SAFE_CALL( cudaFree(pi_d) );
    CUDA_SAFE_CALL( cudaFree(delta_d) );
    CUDA_SAFE_CALL( cudaFree(psi_d) );
    CUDA_SAFE_CALL( cudaFree(inner_prod_d) );
    CUDA_SAFE_CALL( cudaFree(ones_n_d) );
    CUDA_SAFE_CALL( cudaFree(idx_d) );
}
