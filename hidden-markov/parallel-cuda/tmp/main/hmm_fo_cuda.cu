/*
 * Copyright (c) 2011, Shawn Hymel <hymelsr@vt.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * @file
 * @author  Shawn Hymel
 * @date    May 20, 2011
 * @brief   Implements the Forward Algorithm in CUDA
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <cuda.h>
#include <cutil.h>

#include "hmm.h"
extern "C" {    /* required for functions to be visible in C */
#include "hmm_fo_cuda.h"
}

/* Contains information about the graphics card's CUDA capabilities */
enum {
    THREADS_PER_BLOCK = 256,
    MAX_BLOCKS = 65535
};

/*******************************************************************************
 * Kernels
 ******************************************************************************/
 
/* CUDA - Initialize forward variables (alpha) */

 

/* Runs the forward algorithm on the supplied HMM and observation sequence */
float run_hmm_fo(Hmm *in_hmm, Obs *in_obs)
{

    /* Host-side variables */
    int j;
    int k;
    int size;
    int log_lik;
    float *alpha;
    float *scale;
    float *a = in_hmm->a;
    float *b = in_hmm->b;
    float *pi = in_hmm->pi;
    int nstates = in_hmm->nstates;
    int nsymbols = in_hmm->nsymbols;
    int *obs = in_obs->data;
    int length = in_obs->length;
    int threads_per_block = THREADS_PER_BLOCK;
    int nblocks;

    /* Device-side variables */
    float *a_d;
    float *b_d;
    float *pi_d;
    float *alpha_d;
    float *scale_d;
    float *log_lik_d;
    
    /* Check for number of states against maximum allowed */
    if (nstates > THREADS_PER_BLOCK * MAX_BLOCKS) {
        return 1.0f;
    }
    
    /* NEEDED? */
    alpha = (float *) malloc(sizeof(float) * nstates * length);
    scale = (float *) malloc(sizeof(float) * length);
    
    /* Initialize device-side variables */
    size = sizeof(float) * nstates * nstates;
    CUDA_SAFE_CALL( cudaMalloc((void**)&a_d, size) );
    CUDA_SAFE_CALL( cudaMemcpy(a_d, a, size, cudaMemcpyHostToDevice) );
    size = sizeof(float) * nstates * nsymbols;
    CUDA_SAFE_CALL( cudaMalloc((void**)&b_d, size) );
    CUDA_SAFE_CALL( cudaMemcpy(b_d, b, size, cudaMemcpyHostToDevice) );
    size = sizeof(float) * nstates;
    CUDA_SAFE_CALL( cudaMalloc((void**)&pi_d, size) );
    CUDA_SAFE_CALL( cudaMemcpy(pi_d, pi, size, cudaMemcpyHostToDevice) );
    size = sizeof(float) * nstates * length;
    CUDA_SAFE_CALL( cudaMalloc((void**)&alpha_d, size) );
    size = sizeof(float) * length;
    CUDA_SAFE_CALL( cudaMalloc((void**)&scale_d, size) );
    size = sizeof(float);
    CUDA_SAFE_CALL( cudaMalloc((void**)&log_lik_d, size) );
    
    /* Initialize alpha variables */
    nblocks = min(1 + (nstates - 1)/threads_per_block, MAX_BLOCKS);
    /*init_alpha_dev<<<nblocks, threads_per_block>>>( b_d,
                                                    pi_d,
                                                    nstates,
                                                    alpha_d,
                                                    scale_d,
                                                    obs[0]);*/
                                                    
   
   /* Copy results from device to host */
   size = sizeof(float) * nstates * length;
   CUDA_SAFE_CALL( cudaMemcpy(alpha, alpha_d, size, cudaMemcpyDeviceToHost) );
   size = sizeof(float) * length;
   CUDA_SAFE_CALL( cudaMemcpy(scale, scale_d, size, cudaMemcpyDeviceToHost) );
   size = sizeof(float);
   CUDA_SAFE_CALL( cudaMemcpy(&log_lik, log_lik_d, size,
                                                cudaMemcpyDeviceToHost) );
    
    /* TEST */
    printf("alpha:\n");
    for (j = 0; j < length; j++) {
        for (k = 0; k < nstates; k++) {
            printf(" %.4f", alpha[(j * nstates) + k]);
        }
        printf("\n");
    }
    printf("\n");
    
    return log_lik;
    
}
