#include <fstream>   
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <math.h>

using namespace std;

#include <cuda.h>

static void CheckCudaErrorAux (const char *, unsigned, const char *, cudaError_t);
#define CUDA_CHECK_RETURN(value) CheckCudaErrorAux(__FILE__,__LINE__, #value, value)

enum {THREADS_PER_BLOCK = 256};

__device__ float c_d[THREADS_PER_BLOCK * THREADS_PER_BLOCK];


/******************************************************************************/
/* CUDA - Initialize forward variables (beta) */
__global__ void init_beta( float *b_d, float *pi_d, int nstates, float *beta_d, 
                                float *scale_d,int obs_t)
{
    unsigned int idx = threadIdx.x + blockIdx.x * blockDim.x;
    
    /* Initialize beta values for first observation */
    if (idx < nstates) {
        beta_d[idx] = 1.0f;
    }
}


/******************************************************************************/
/* CUDA - Calculate beta variables */
__global__ void calc_beta( float *beta_d, float *a_d, float *b_d, int nstates, int obs_t, int t)
{
    unsigned int tid = threadIdx.x;
    unsigned int bid = blockIdx.x;
    unsigned int offset;
    __shared__ float partial_sum[THREADS_PER_BLOCK];
    
    /* Calculate individual C values */
    if (tid < nstates && bid < nstates) {
        c_d[(bid * nstates) + tid] = beta_d[((t-1) * nstates) + tid] * a_d[(tid * nstates) + bid] * b_d[(obs_t * nstates) + bid];   
    }
    
    /* Fill partial sums in each block */
    if (tid < nstates && bid < nstates) {
        partial_sum[tid] = c_d[(bid * nstates) + tid];
    } else {
        partial_sum[tid] = 0;
    }
    
    /* Perform reduction on partial sum in each block */
    __syncthreads();
    for (offset = blockDim.x/2; offset > 0; offset /= 2) {
        if (tid < offset) {
            partial_sum[tid] = partial_sum[tid] + partial_sum[tid + offset];
        }
        __syncthreads();
    }
    
    if (tid == 0 && bid < nstates) {
        beta_d[(t * nstates) + bid] = partial_sum[0];
    }
}


/******************************************************************************/
/* CUDA - Scale beta values */
__global__ void scale_beta(    float *beta_d, float *scale_d, int nstates,int t)
{
    unsigned int tid = threadIdx.x;
    unsigned int offset;
    __shared__ float partial_sum[THREADS_PER_BLOCK];
    
    /* Calculate scaling factor to prevent underflow - use reduction for sum */
    if (tid < nstates) {
        partial_sum[tid] = beta_d[(t * nstates) + tid];
    } else {
        partial_sum[tid] = 0;
    }
    __syncthreads();
    for (offset = blockDim.x/2; offset > 0; offset /= 2) {
        if (tid < offset) {
            partial_sum[tid] += partial_sum[tid + offset];
        }
        __syncthreads();
    }
    if (tid == 0) {
        scale_d[t] = partial_sum[0];
    }
    
    /* Scale beta values */
    if (tid < nstates) {
        beta_d[(t * nstates) + tid] = beta_d[(t * nstates) + tid] / scale_d[t]; 
    }
}


/******************************************************************************/
/* Runs the forward algorithm on the supplied HMM and observation sequence */
int main()
{
    /* Host-side variables */
    int size, j , k;
    int nsymbols;
    int nstates;
    int length;
    float *beta;
    float *scale;
    
    std::ifstream inputHMM("./res/2coins");
    inputHMM >> nstates >> nsymbols;
    cout << "nstates: " << nstates << "\t nsymbols: " << nsymbols << "\n";

    float *a = new float[nstates*nstates];
    float *b = new float[nstates*nsymbols];
    float *pi = new float[nstates];

    for (j = 0; j < nstates; j++) {
        for (k = 0; k < nstates; k++) {
            inputHMM >> a[(j * nstates) + k];
        }
    }

    for (j = 0; j < nstates; j++) {
        for (k = 0; k < nsymbols; k++) {
            inputHMM >> b[(j * nstates) + k];
        }
    }

    for (j = 0; j < nstates; j++) {
        inputHMM >> pi[j];
    }
    
    inputHMM.close();
    
    std::ifstream inputObs("./res/2coins-10000obs");
    inputObs >> length;
    cout << "\nlength: " << length << endl;

    int   *obs = new int[length];
    
    for (j = 0; j < length; j++) {
          inputObs >> obs[j];
    }
    inputObs.close();

    /* Print A */
    cout << "# state transition probability (A)\n";
    for (j = 0; j < nstates; j++) {
        for (k = 0; k < nstates; k++) {
            //a[(j * nstates) + k] = 0.5;
            cout << a[(j * nstates) + k] << "\t";
        } cout << "\n";
    } cout << "\n\n";

    /* Print B */
    cout << "# state output probility (B)\n";
    for (j = 0; j < nstates; j++) {
        for (k = 0; k < nsymbols; k++) {
            //b[(j * nstates) + k] = 0.5;
            cout << b[(j * nstates) + k] << "\t";
        } cout << "\n";
    } cout << "\n\n";

    /* Print Pi */
    cout << "# initial state probability (Pi)\n";
    for (j = 0; j < nstates; j++) {
        //pi[j] = 0.5;
        cout << pi[j] << "\t";
    } cout << "\n\n";

    /* Print Obs */
    // cout << "# observations sequence (Obs)\n";
    // for (j = 0; j < length; j++) {
    //     cout << obs[j] << " ";
    // } cout << "\n\n";

    int threads_per_block = THREADS_PER_BLOCK;
    int nblocks;
    int t;
    
    /* Device-side variables */
    float *a_d;
    float *b_d;
    float *pi_d;
    float *obs_d;
    float *beta_d;
    float *scale_d;   
    
    beta = new float[ nstates * length];
    scale = new float[length];
    
    /* Check for number of states against maximum allowed */
    if (nstates > THREADS_PER_BLOCK) {
        return EXIT_FAILURE;
    }
    
    /* Allocate memory and copy needed parameters to device */
    size = sizeof(float) * nstates * nstates;
      CUDA_CHECK_RETURN(cudaMalloc((void**)&a_d, size) );
      CUDA_CHECK_RETURN(cudaMemcpy(a_d, a, size, cudaMemcpyHostToDevice)) ;
    size = sizeof(float) * nstates * nsymbols;
      CUDA_CHECK_RETURN(cudaMalloc((void**)&b_d, size) );
      CUDA_CHECK_RETURN(cudaMemcpy(b_d, b, size, cudaMemcpyHostToDevice) );
    size = sizeof(float) * nstates;
      CUDA_CHECK_RETURN(cudaMalloc((void**)&pi_d, size));
      CUDA_CHECK_RETURN(cudaMemcpy(pi_d, pi, size, cudaMemcpyHostToDevice)) ;
    size = sizeof(float) * length;
      CUDA_CHECK_RETURN(cudaMalloc((void**)&obs_d, size));
      CUDA_CHECK_RETURN(cudaMemcpy(obs_d, obs, size, cudaMemcpyHostToDevice)) ;
    size = sizeof(float) * nstates * length;
      CUDA_CHECK_RETURN(cudaMalloc((void**)&beta_d, size) );
    size = sizeof(float) * length;
      CUDA_CHECK_RETURN(cudaMalloc((void**)&scale_d, size)) ;
    
    /* Initialize beta variables */
    init_beta<<<1, threads_per_block>>>(   b_d, pi_d, nstates, beta_d,  scale_d, obs[0]);
                                              
    /* Calculate the rest of the beta variables */
    nblocks = threads_per_block;
    for (t = 1; t < length; t++) {
        calc_beta<<<nblocks, threads_per_block>>>( beta_d, a_d, b_d, nstates, obs[t], t);
        scale_beta<<<1, threads_per_block>>>(  beta_d, scale_d, nstates, t);
    }
    
    /* Copy results from device to host */
    size = sizeof(float) * nstates * length;                                      
      CUDA_CHECK_RETURN(cudaMemcpy(beta, beta_d, size, cudaMemcpyDeviceToHost));
    size = sizeof(float) * length;
      CUDA_CHECK_RETURN(cudaMemcpy(scale, scale_d, size, cudaMemcpyDeviceToHost)) ;
    
                                                    

//printf("# beta\n");
//    for (j = 0; j < length; j++) {
//        for (k = 0; k < nstates; k++) {
//            printf(" %.4f", beta[(j * nstates) + k]);
//        }
//        printf("\n");
//    }
    
	/* Free device memory */
	CUDA_CHECK_RETURN(cudaFree(a_d) );
	CUDA_CHECK_RETURN(cudaFree(b_d) );
	CUDA_CHECK_RETURN(cudaFree(pi_d)) ;
	CUDA_CHECK_RETURN(cudaFree(obs_d)) ;
	CUDA_CHECK_RETURN(cudaFree(beta_d)) ;
	CUDA_CHECK_RETURN(cudaFree(scale_d)) ;
    
    CUDA_CHECK_RETURN(cudaDeviceReset()); 

    delete [] a;
    delete [] b;
    delete [] pi;
    delete [] obs;
    delete [] beta;
    delete [] scale;

    return EXIT_SUCCESS;
}

static void CheckCudaErrorAux (const char *file, unsigned line, const char *statement, cudaError_t err)
{
	if (err == cudaSuccess)
		return;	cerr << statement<<" returned " << cudaGetErrorString(err) << "("<<err<< ") at "<<file<<":"<<line << endl;
	exit (1);
}
