#include <fstream>   
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <math.h>

using namespace std;

#include <cuda.h>

enum {
    THREADS_PER_BLOCK = 256     /* Note: must be a power of 2 for reductions */
};

__device__ float c_d[THREADS_PER_BLOCK * THREADS_PER_BLOCK];


/******************************************************************************/
/* CUDA - Initialize forward variables (alpha) */
__global__ void init_alpha_dev( float *b_d, float *pi_d, int nstates, float *alpha_d, 
                                float *scale_d, float *log_lik_d,int obs_t)
{
    unsigned int tid = threadIdx.x;
    unsigned int idx = blockIdx.x * blockDim.x + tid;
    unsigned int stride;
    __shared__ float partial_sum[THREADS_PER_BLOCK];
    
    /* Initialize alpha values for first observation */
    if (idx < nstates) {
        alpha_d[idx] = pi_d[idx] * b_d[(obs_t * nstates) + idx];
    }
    
    /* Calculate scaling factor to prevent underflow - use reduction for sum */
    if (idx < nstates) {
        partial_sum[tid] = alpha_d[tid];
    } else {
        partial_sum[tid] = 0;
    }
    __syncthreads();
    for (stride = blockDim.x/2; stride > 0; stride /= 2) {
        if (tid < stride) {
            partial_sum[tid] += partial_sum[tid + stride];
        }
        __syncthreads();
    }
    if (tid == 0) {
        scale_d[0] = partial_sum[0];
    }
    
    /* Scale initial alpha values */
    if (idx < nstates) {
        alpha_d[idx] = alpha_d[idx]/scale_d[0]; /* OPT: use const memory? */
    }
    
    /* Initialize log likelihood */
    if (tid == 0) {
        *log_lik_d = log10(scale_d[0]);
    }

}


/******************************************************************************/
/* CUDA - Calculate alpha variables */
__global__ void calc_alpha_dev( float *alpha_d, float *a_d, float *b_d, int nstates, int obs_t, int t)
{
    unsigned int tid = threadIdx.x;
    unsigned int bid = blockIdx.x;
    unsigned int stride;
    __shared__ float partial_sum[THREADS_PER_BLOCK];
    
    /* Calculate individual C values */
    if (tid < nstates && bid < nstates) {
        c_d[(bid * nstates) + tid] = alpha_d[((t-1) * nstates) + tid] * 
                                        a_d[(tid * nstates) + bid] *
                                        b_d[(obs_t * nstates) + bid];   
    }
    
    /* Fill partial sums in each block */
    if (tid < nstates && bid < nstates) {
        partial_sum[tid] = c_d[(bid * nstates) + tid];
    } else {
        partial_sum[tid] = 0;
    }
    
    /* Perform reduction on partial sum in each block */
    __syncthreads();
    for (stride = blockDim.x/2; stride > 0; stride /= 2) {
        if (tid < stride) {
            partial_sum[tid] = partial_sum[tid] + partial_sum[tid + stride];
        }
        __syncthreads();
    }
    
    /* Copy sums to alpha */
    if (tid == 0 && bid < nstates) {
        alpha_d[(t * nstates) + bid] = partial_sum[0];
    }
}


/******************************************************************************/
/* CUDA - Scale alpha values */
__global__ void scale_alpha_dev(    float *alpha_d, float *scale_d, int nstates,int t, float *log_lik_d)
{
    unsigned int tid = threadIdx.x;
    unsigned int stride;
    __shared__ float partial_sum[THREADS_PER_BLOCK];
    
    /* Calculate scaling factor to prevent underflow - use reduction for sum */
    if (tid < nstates) {
        partial_sum[tid] = alpha_d[(t * nstates) + tid];
    } else {
        partial_sum[tid] = 0;
    }
    __syncthreads();
    for (stride = blockDim.x/2; stride > 0; stride /= 2) {
        if (tid < stride) {
            partial_sum[tid] += partial_sum[tid + stride];
        }
        __syncthreads();
    }
    if (tid == 0) {
        scale_d[t] = partial_sum[0];
    }
    
    /* Scale alpha values */
    if (tid < nstates) {
        alpha_d[(t * nstates) + tid] = alpha_d[(t * nstates) + tid] /
                                        scale_d[t]; /* OPT: use const memory? */
    }
       
    /* Update log likelihood */
    if (tid == 0) {
        *log_lik_d += log10(scale_d[t]);
    }
}


/******************************************************************************/
/* Runs the forward algorithm on the supplied HMM and observation sequence */
int main()
{
    /* Host-side variables */
    int size, j , k;
    int nsymbols;
    int nstates;
    int length;
    float *alpha;
    float *scale;
    
    std::ifstream inputHMM("./res/2coins");
    inputHMM >> nstates >> nsymbols;
    cout << "nstates: " << nstates << "\t nsymbols: " << nsymbols << "\n";

    float *a = new float[nstates*nstates];
    float *b = new float[nstates*nsymbols];
    float *pi = new float[nstates];

    for (j = 0; j < nstates; j++) {
        for (k = 0; k < nstates; k++) {
            inputHMM >> a[(j * nstates) + k];
        }
    }

    for (j = 0; j < nstates; j++) {
        for (k = 0; k < nsymbols; k++) {
            inputHMM >> b[(j * nstates) + k];
        }
    }

    for (j = 0; j < nstates; j++) {
        inputHMM >> pi[j];
    }
    
    inputHMM.close();
    
    std::ifstream inputObs("./res/2coins-100obs");
    inputObs >> length;
    cout << "\nlength: " << length << endl;

    int   *obs = new int[length];
    
    for (j = 0; j < length; j++) {
          inputObs >> obs[j];
    }
    inputObs.close();

    /* Print A */
    cout << "# state transition probability (A)\n";
    for (j = 0; j < nstates; j++) {
        for (k = 0; k < nstates; k++) {
            //a[(j * nstates) + k] = 0.5;
            cout << a[(j * nstates) + k] << "\t";
        } cout << "\n";
    } cout << "\n\n";

    /* Print B */
    cout << "# state output probility (B)\n";
    for (j = 0; j < nstates; j++) {
        for (k = 0; k < nsymbols; k++) {
            //b[(j * nstates) + k] = 0.5;
            cout << b[(j * nstates) + k] << "\t";
        } cout << "\n";
    } cout << "\n\n";

    /* Print Pi */
    cout << "# initial state probability (Pi)\n";
    for (j = 0; j < nstates; j++) {
        //pi[j] = 0.5;
        cout << pi[j] << "\t";
    } cout << "\n\n";

    /* Print Obs */
    // cout << "# observations sequence (Obs)\n";
    // for (j = 0; j < length; j++) {
    //     cout << obs[j] << " ";
    // } cout << "\n\n";

    int threads_per_block = THREADS_PER_BLOCK;
    int nblocks;
    int t;
    float log_lik;
    
    /* Device-side variables */
    float *a_d;
    float *b_d;
    float *pi_d;
    float *obs_d;
    float *alpha_d;
    float *scale_d;
    float *log_lik_d;
    
    /* TODO: Needed? */
    alpha = new float[ nstates * length];
    scale = new float[length];
    
    /* Check for number of states against maximum allowed */
    if (nstates > THREADS_PER_BLOCK) {
        return 1.0f;
    }
    
    /* Allocate memory and copy needed parameters to device */
    size = sizeof(float) * nstates * nstates;
      cudaMalloc((void**)&a_d, size) ;
      cudaMemcpy(a_d, a, size, cudaMemcpyHostToDevice) ;
    size = sizeof(float) * nstates * nsymbols;
      cudaMalloc((void**)&b_d, size) ;
      cudaMemcpy(b_d, b, size, cudaMemcpyHostToDevice) ;
    size = sizeof(float) * nstates;
      cudaMalloc((void**)&pi_d, size);
      cudaMemcpy(pi_d, pi, size, cudaMemcpyHostToDevice) ;
    size = sizeof(float) * length;
      cudaMalloc((void**)&obs_d, size);
      cudaMemcpy(obs_d, obs, size, cudaMemcpyHostToDevice) ;
    size = sizeof(float) * nstates * length;
      cudaMalloc((void**)&alpha_d, size) ;
    size = sizeof(float) * length;
      cudaMalloc((void**)&scale_d, size) ;
    size = sizeof(float);
      cudaMalloc((void**)&log_lik_d, size) ;
    
    /* Initialize alpha variables */
    init_alpha_dev<<<1, threads_per_block>>>(   b_d, pi_d, nstates, alpha_d,  scale_d, log_lik_d, obs[0]);
                                              
    /* Calculate the rest of the alpha variables */
    nblocks = threads_per_block;
    for (t = 1; t < length; t++) {
        calc_alpha_dev<<<nblocks, threads_per_block>>>( alpha_d, a_d, b_d, nstates, obs[t], t);
        scale_alpha_dev<<<1, threads_per_block>>>(  alpha_d, scale_d, nstates, t, log_lik_d);
    }
    
    /* Copy results from device to host */
    size = sizeof(float) * nstates * length;                                      
      cudaMemcpy(alpha, alpha_d, size, cudaMemcpyDeviceToHost);
    size = sizeof(float) * length;
      cudaMemcpy(scale, scale_d, size, cudaMemcpyDeviceToHost) ;
    size = sizeof(float);
      cudaMemcpy(&log_lik, log_lik_d, size, 
                                                    cudaMemcpyDeviceToHost) ;
                                                    

//	printf("# alpha\n");
//    for (j = 0; j < length; j++) {
//        for (k = 0; k < nstates; k++) {
//            printf(" %.4f", alpha[(j * nstates) + k]);
//        }
//        printf("\n");
//    }
    
    /* Free device memory */
      cudaFree(a_d) ;
      cudaFree(b_d) ;
      cudaFree(pi_d) ;
      cudaFree(obs_d) ;
      cudaFree(alpha_d) ;
      cudaFree(scale_d) ;
    
//    return log_lik;  
    cudaDeviceReset(); 
	return 0;
}

