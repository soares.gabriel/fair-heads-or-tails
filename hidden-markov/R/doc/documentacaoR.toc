\select@language {brazil}
\contentsline {section}{\numberline {1}Descri\IeC {\c c}\IeC {\~a}o}{3}
\contentsline {section}{\numberline {2}Estrutura do Modelo}{3}
\contentsline {section}{\numberline {3}Documenta\IeC {\c c}\IeC {\~a}o das Fun\IeC {\c c}\IeC {\~o}es}{3}
\contentsline {subsection}{\numberline {3.1} Backward, logBackward e scaleBackward }{3}
\contentsline {subsubsection}{\numberline {3.1.1}Descri\IeC {\c c}\IeC {\~a}o}{3}
\contentsline {subsubsection}{\numberline {3.1.2} Uso}{4}
\contentsline {subsubsection}{\numberline {3.1.3}Argumentos}{4}
\contentsline {subsubsection}{\numberline {3.1.4}Retorno}{4}
\contentsline {subsection}{\numberline {3.2} BaumWelch e logBaumWelch }{4}
\contentsline {subsubsection}{\numberline {3.2.1}Descri\IeC {\c c}\IeC {\~a}o}{4}
\contentsline {subsubsection}{\numberline {3.2.2} Uso}{4}
\contentsline {subsubsection}{\numberline {3.2.3}Argumentos}{5}
\contentsline {subsubsection}{\numberline {3.2.4}Retorno}{5}
\contentsline {subsection}{\numberline {3.3} Forward, logForward e scaleForward }{5}
\contentsline {subsubsection}{\numberline {3.3.1}Descri\IeC {\c c}\IeC {\~a}o}{5}
\contentsline {subsubsection}{\numberline {3.3.2} Uso}{5}
\contentsline {subsubsection}{\numberline {3.3.3}Argumentos}{5}
\contentsline {subsubsection}{\numberline {3.3.4}Retorno}{5}
\contentsline {subsection}{\numberline {3.4}inicializarHMM}{6}
\contentsline {subsubsection}{\numberline {3.4.1}Descri\IeC {\c c}\IeC {\~a}o}{6}
\contentsline {subsubsection}{\numberline {3.4.2} Uso}{6}
\contentsline {subsubsection}{\numberline {3.4.3}Argumentos}{6}
\contentsline {subsubsection}{\numberline {3.4.4}Retorno}{6}
