# PIBIC 2014/2015 - PVB2244-2014 - Paralelização de Algoritmos de Treinamento de Modelos Escondidos de Markov
## Implementação em R de treinamento de HMM

Além de uma implementação simples, os algoritmos _Forward_, _Backward_, _Viterbi_ e _Baum-Welch_ também foram implementados seguindo duas abordagens comuns para lidar com valores diminutos das probabilidades condicionais (_underflow_). A primeira consiste em redimensionar as probabilidades condicionais usando fatores de escala cuidadosamente projetados (as funções que implementam essa abordagem possuem o prefixo `scale`), e a segunda trabalha com os logaritmos das probabilidades condicionais e suas propriedades (as funções que implementam essa abordagem possuem o prefixo `log`).

