﻿/*
 * main.cpp
 * 
 * Copyright 2015 ???????
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "hmm.hpp"
#include <fstream>      /* ifstream */
#include <cstdlib>      /* EXIT_FAILURE */
#include <assert.h>   /* assert */
#include <math.h>     /* round, floor, ceil, trunc */
#include <iostream>     /* cout */

//using namespace std;

/*
 * 
 * name: printMulDimtArray
 * @param double** array, int T, int N
 * @return None
 * 
 */

void printMulDimtArray(double** array, int T, int N)
{
	for (int i = 0; i < T; ++i) {
        for (int j = 0; j < N; ++j) {
            std::cout << "\t" << array[i][j];
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


/*
 * 
 * name: readObservations
 * @param Input file, int T = 0
 * @return Vector with T observations from file
 * 
 */

int* readObservations(std::ifstream& input, int& T)
{
    input >> T;
    std::cout << "Sequence of "<< T << " observations \n\n";
    int *O = new int [T];
    for (int i = 0; i < T; i++)
    {
          input >> O[i];
          //std::cout << O[i] << " ";
    } //std::cout << std::endl;
    return O;
}

double** _allocMatrix(int T, int N)
{
    double** matrix = new double*[T];
    for(int i = 0; i < T; i++)
        matrix[i] = new double[N];

    return matrix;
}

void _deallocMatrix(double** matrix)
{
    delete [] matrix[0];
    delete [] matrix;
}

int main ()
{
    int T = 0;
    std::ifstream inputHMM("2dice1coin");
    HMM hmm(inputHMM);
    inputHMM.close();
    hmm.print();

    std::ifstream inputObs("2dice1coin-500obs");
    int *O = readObservations(inputObs, T);
    inputObs.close();


//    double **alpha = _allocMatrix(T,hmm.getN());
//    double **beta = _allocMatrix(T,hmm.getN());
//    double probf = 0.0,probb = 0.0;
//    hmm.Forward(T, O, alpha,probf);
//    std::cout << "\nalpha\n" << std::endl;
//    printMulDimtArray(alpha,4,3);

//    hmm.Backward(T, O, beta,probb);
//    std::cout << "\nbeta\n" << std::endl;
//    printMulDimtArray(beta,4,3);
//    //std::cout << probf << " == " << probb << std::endl;
//    //assert(probf == probb);
//    _deallocMatrix(alpha); _deallocMatrix(beta);



//    double **alpha = _allocMatrix(T,hmm.getN()), **beta = _allocMatrix(T,hmm.getN());
//    double*  scale = new double[T];
//    double prob;
//    hmm.ForwardScale(T, O, alpha, scale, prob);
//    hmm.BackwardScale(T, O, beta, scale);
//    //std::cout << exp(prob) << std::endl;
//    delete [] scale;
//    _deallocMatrix(alpha); _deallocMatrix(beta);


    /*
    int *q = new int[T];
    double prob;
    //hmm.Viterbi(T,O,q,prob);
    hmm.ViterbiLog(T,O,q,prob);
    for(int i = 0; i < T; i++)
        std::cout << q[i] << " ";
    std::cout << std::endl << prob << std::endl;
    */


    double **alpha, **beta;
    hmm.BaumWelch(T, O, alpha,beta);

    delete [] O;

    return EXIT_SUCCESS;
}


//int main (int argc, char *argv[])
//{

//    int T = 0, *O;
//    double **alpha, **beta;
//    if ( argc != 3 ){
//        //std::cerr <<"Usage: "<< argv[0] <<" <HMM filename>"<<" <OBS filename>\n";
//        std::cerr <<"\n>> Usage: "<< "HMM [Hidden Markov Model FILE]... [Observations FILE]... [OPTIONS]\n"
//                                     "Modelling, analysis and inference with discrete time Hidden Markov Models.\n\n";
//        exit(1);
//    } else {
//        std::ifstream inputHMM(argv[1]);
//        std::ifstream inputObs(argv[2]);
//        if ( !inputHMM.is_open() ){
//            std::cerr <<">> Could not open the file called \"" << argv[1] << "\" for reading.\n";
//            exit(1);
//        } else {
//            std::cout << "\n>> Model \"" << argv[1] << "\" LOADED.\n\n";
//            HMM hmm(inputHMM);
//            hmm.print();
//            inputHMM.close();
//            if ( !inputObs.is_open() ){
//                std::cerr <<">> Could not open the file called \"" << argv[2] << "\" for reading.\n";
//                exit(1);
//            } else {
//                std::cout << "\n>> Observations from \"" << argv[2] << "\" LOADED.\n\n";
//                O = readObservations(inputObs, T);
//                inputObs.close();

////                hmm.Backward(T,O,beta, probb);
////                hmm.BackwardScale(T,O,beta, scale);
////                hmm.Forward(T, O, alpha, probf);
////                hmm.ForwardScale(T, O , alpha, scale, probf);
////                hmm.Viterbi(T, O, q, prob);
////                hmm.ViterbiLog(T, O, q, prob);
//                  hmm.BaumWelch(T, O, alpha,beta);
//            }
//            delete [] O;
//        }
//    } /* argc == 3 */
//    return EXIT_SUCCESS;
//}
