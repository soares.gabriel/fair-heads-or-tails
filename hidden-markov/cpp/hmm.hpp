/*
 * hmm.hpp
 * 
 * Copyright 2015 ?????
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <fstream>      /* ifstream */
#include <math.h>       /* log */
#include <limits>       /* std::numeric_limits */
#include <iostream>     /* std::cout */

#ifndef HMM_H
#define HMM_H

#define DELTA 0.01
#define MAXVALUE std::numeric_limits<double>::max()

class HMM
{
private:
    int N;
    int M;
    double	**A;
    double	**B;
    double	*pi;
    void initHMM();             /* 2 coins HMM initialization */
    void Gamma      (int T, double **alpha, double **beta,double **gamma);
    void Xi         (int T, int *O, double **alpha, double **beta,double ***xi);
public:
    HMM();                      /* default - 2 coins HMM */
    HMM(int, int);              /* given N and M initialize the parameters with 0 */
    HMM(std::ifstream& );       /* read a valid HMM from file */
    ~HMM();
    void setN(int n);
    void setM(int m);
    int getN();
    int getM();
    void print();
    HMM operator=(const HMM*);
    HMM operator=(const HMM&);

    void Forward        (int T, int *O, double **alpha, double &prob);
    void ForwardScale   (int T, int *O, double **alpha, double *scale, double &prob);

    void Backward       (int T, int *O, double **beta, double &prob);
    void BackwardScale  (int T, int *O, double **beta,	double *scale);

    void Viterbi    (int T, int *O, int *q, double &prob);
    void ViterbiLog (int T, int *O, int *q, double &prob);

    void BaumWelch  (int T, int *O, double **alpha, double **beta);
};

#endif // HMM_H
