/*
 * hmm.cpp
 *
 * Copyright 2015 ???????????
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "hmm.hpp"

double** allocMatrix(int T, int N)
{
    double** matrix = new double*[T];
    for(int i = 0; i < T; i++)
        matrix[i] = new double[N];

    return matrix;
}

void deallocMatrix(double** matrix)
{
    delete [] matrix[0];
    delete [] matrix;
}

HMM::HMM()
{
    N = 2;
    M = 2;
    A = allocMatrix(N, N);
    B = allocMatrix(N, N);
    pi = new double[N];

    initHMM();
}

HMM::HMM(int n, int m)
{
    N = n;
    M = m;

    A = allocMatrix(N, N);
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            A[i][j] = 0.0;
        }
    }
    B = allocMatrix(N, M);
    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j++){
            B[i][j] = 0.0;
        }
    }
    pi = new double[N];
    for (int i = 0; i < N; i++)
    {
        pi[i] = 0.0;
    }

}

HMM::HMM(std::ifstream& input)
{
    input >> N >> M;

    A = allocMatrix(N, N);
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            input >> A[i][j];
        }
    }
    B = allocMatrix(N, M);
    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j++){
            input >> B[i][j];
        }
    }
    pi = new double[N];
    for (int i = 0; i < N; i++)
    {
        input >> pi[i];
    }
}

HMM::~HMM()
{
    deallocMatrix(B);
    deallocMatrix(A);
    delete [] pi;
}

void HMM::print()
{
    std::cout << "Number of states: " << this->N << "\n";

    std::cout << "Number of symbols: " << this->M << "\n\n";

    std::cout << "Starting probabilities vector\n\n";
    for (int i = 0; i < N; i++)
    {
        std::cout << "\t" << pi[i];
    }  std::cout << "\n\n";

    std::cout << "Transition probabilities matrix\n\n";
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            std::cout << " \t" << A[i][j];
        } std::cout << "\n";
    } std::cout << "\n";

    std::cout << "Emission probabilities matrix\n\n";
    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j++){
            std::cout << " " << B[i][j];
        } std::cout << "\n";
    } std::cout << "\n";
}

void HMM::initHMM()
{    
    A[0][0] = 0.25;
    A[0][1] = 0.75;
    A[1][0] = 0.7;
    A[1][1] = 0.3;

    B[0][0] = 0.5;
    B[0][1] = 0.5;
    B[1][0] = 0.9;
    B[1][1] = 0.1;

    pi[0] = 0.5;
    pi[1] = 0.5;
}

void HMM::setN(int n) { N = n; }
void HMM::setM(int m) { M = m; }
int HMM::getN() { return N; }
int HMM::getM() { return M; }

HMM HMM::operator=(const HMM *a)
{
    HMM b(a->N, a->M);
    for (int i = 0; i < b.N; i++){
        b.pi[i] = a->pi[i];
        for (int j = 0; j < b.N; j++){
           b.A[i][j] = a->A[i][j];
        }
    }
    for (int i = 0; i < b.N; i++){
        for (int j = 0; j < b.M; j++){
           b.B[i][j] = a->B[i][j];
        }
    }
    return b;
}

HMM HMM::operator=(const HMM& a)
{
    HMM b;
    b.N = a.N; b.M = a.M;
    for (int i = 0; i < b.N; i++){
        b.pi[i] = a.pi[i];
        for (int j = 0; j < b.N; j++){
           b.A[i][j] = a.A[i][j];
        }
    }
    for (int i = 0; i < b.M; i++){
        for (int j = 0; j < b.N; j++){
           b.B[i][j] = a.B[i][j];
        }
    }
    return b;
}

void print1(double** array, int T, int N)
{
    for (int i = 0; i < T; ++i) {
        for (int j = 0; j < N; ++j) {
            std::cout << "\t" << array[i][j];
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

/*
 *
 * name: Forward
 * @param
 * @return
 * @description
 *
 */
void HMM::Forward(int T, int *O, double **alpha, double &prob)
{
    double sum;

//    std::cout << "/* Forward */\n";
//    std::cout << "/* 1. Initialization */\n";

    for (int i = 0; i < N; i++){
        alpha[0][i] = pi[i]*B[i][O[0]];
        std::cout << pi[i] << "*" << B[i][O[0]] << " = "<< alpha[0][i] <<"\n";
    } std::cout <<"\n";

//    std::cout << "/* 2. Induction */\n";

    for (int t = 0; t < T-1; t++) {
        for (int j = 0; j < N; j++) {
            sum = 0.0;
            for (int i = 0; i < N; i++){
                sum += alpha[t][i]*(A[i][j]);
                std::cout << alpha[t][i] << "*" << A[i][j] << " = "<< sum <<"\n";
            }

            alpha[t+1][j] = sum*(B[j][O[t+1]]);
            std::cout << sum << "*" << B[j][O[t+1]] << " = "<< alpha[t+1][j] <<"\n\n";
        }
    }

//    std::cout << "/* 3. Termination */\n";
    prob = 0.0;
    for (int i = 0; i < N; i++){
        prob += alpha[T-1][i];
//        std::cout << alpha[T-1][i] <<" + ";
    } /*std::cout << "\n" <<  prob << "\n";*/

}

/*
 *
 * name: ForwardScale
 * @param
 * @return
 * @description
 *
 */
void HMM::ForwardScale(int T, int *O, double **alpha, double *scale, double &prob)
{
    double sum;

    //std::cout << "/* Forward Scale*/\n";
    //std::cout << "/* 1. Initialization */\n";
    scale[0] = 0.0;
    for (int i = 0; i < N; i++) {
        alpha[0][i] = pi[i]* (B[i][O[0]]);
        //std::cout << pi[i] << "*" << B[i][O[0]] << " = "<< alpha[0][i] <<"\n";
        scale[0] += alpha[0][i];
    } //std::cout <<"\n";
    for (int i = 0; i < N; i++){
        alpha[0][i] /= scale[0];
        //std::cout << "alpha/scale = "<< alpha[0][i] <<"\n";
    }

    //std::cout << "/* 2. Induction */\n";

    for (int t = 0; t < T - 1; t++) {
        scale[t+1] = 0.0;
        for (int j = 0; j < N; j++) {
            sum = 0.0;
            for (int i = 0; i < N; i++){
                sum += alpha[t][i]* (A[i][j]);
                //std::cout << alpha[t][i] << "*" << A[i][j] << " = "<< sum <<"\n";
            }

            alpha[t+1][j] = sum*(B[j][O[t+1]]);
            scale[t+1] += alpha[t+1][j];
        }
        for (int j = 0; j < N; j++){
            alpha[t+1][j] /= scale[t+1];
            //std::cout << "alpha/scale = "<< alpha[t+1][j] <<"\n";
        }
    }

    //    std::cout << "/* 3. Termination */\n";
    prob = 0.0;
    for (int t = 0; t < T; t++){
        prob += log(scale[t]);
        //std::cout << log(scale[t]) <<" + ";
    } //std::cout << "\n" <<  exp(prob) << "\n";
}

/*
 *
 * name: Backward
 * @param
 * @return
 * @description
 *
 */
void HMM::Backward(int T, int *O, double **beta, double &prob)
{
    double sum;

    std::cout << "/* Backward */\n";
    std::cout << "/* 1. Initialization */\n\t";

    for (int i = 0; i < N; i++){
        beta[T-1][i] = 1.0;
        std::cout << beta[T-1][i] <<"\t";
    } std::cout << "\n";

    std::cout << "/* 2. Induction */\n";

    for (int t = T-2; t >= 0; t--) {
        for (int i = 0; i < N; i++) {
            sum = 0.0;
            for (int j = 0; j < N; j++){
                sum += A[i][j]*(B[j][O[t+1]])*beta[t+1][j];
                std::cout << A[i][j] << "*" << B[j][O[t+1]] << "*" << beta[t+1][j] << " = "<< sum <<"\n";
            }

            beta[t][i] = sum;
            std::cout << beta[t][i] <<" = " << sum << "\n\n";
        }
    }

//    std::cout << "/* 3. Termination */\n";
    prob = 0.0;
    for (int i = 0; i < N; i++){
        prob += pi[i]*B[i][O[0]]*beta[0][i];
//    std::cout << pi[i] << "*" << B[i][O[0]] << "*" << beta[0][i] <<" + ";
    } /*std::cout << "\n" << prob << "\n";*/
}

/*
 *
 * name: BackwardScale
 * @param
 * @return
 * @description
 *
 */
void HMM::BackwardScale(int T, int *O, double **beta, double *scale)
{
    double sum;

    //std::cout << "/* Backward Scale*/\n";
    //std::cout << "/* 1. Initialization */\n";

    for (int i = 0; i < N; i++){
        beta[T-1][i] = 1.0/scale[T-1];
        //std::cout << beta[T-1][i] << " scale = " << scale[T-1] << "\t";
    } //std::cout << "\n";

    //std::cout << "/* 2. Induction */\n";

    for (int t = T - 2; t >= 0; t--) {
        for (int i = 0; i < N; i++) {
            sum = 0.0;
            for (int j = 0; j < N; j++){
                sum += A[i][j] *(B[j][O[t+1]])*beta[t+1][j];
                //std::cout << A[i][j] << "*" << B[j][O[t+1]] << "*" << beta[t+1][j] << " = "<< sum <<"\n";
            }
            beta[t][i] = sum/scale[t];
            //std::cout << "\n" <<  beta[t][i];
            //std::cout << " scale = " <<  scale[t] << "\n\n";
        }
    }
}

/*
 *
 * name: Gamma
 * @param
 * @return
 * @description
 *
 */
void HMM::Gamma(int T, double **alpha, double **beta,double **gamma)
{
    double	denominator;

    for (int t = 0; t < T; t++) {
        denominator = 0.0;
        for (int j = 0; j < N; j++) {
            gamma[t][j] = alpha[t][j]*beta[t][j];
            denominator += gamma[t][j];
        }

        for (int i = 0; i < N; i++)
            gamma[t][i] = gamma[t][i]/denominator;
    }
}

/*
 *
 * name: Xi
 * @param
 * @return
 * @description
 *
 */
void HMM::Xi(int T, int *O, double **alpha, double **beta,double ***xi)
{
    double sum;

    for (int t = 0; t < T - 1; t++) {
        sum = 0.0;
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++) {
                xi[t][i][j] = alpha[t][i]*beta[t+1][j]*(A[i][j])*(B[j][O[t+1]]);
                //std::cout << "xi[" << t << "][" << i << "][" << j << "] = " << xi[t][i][j] << "\n";
                sum += xi[t][i][j];
                //std::cout << "sum = " << sum << "\n\n";
            }

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++){
                xi[t][i][j]  /= sum;
                //std::cout << "xi[" << t << "][" << i << "][" << j << "] = " << xi[t][i][j] << "\n";
            }
    }
}

double*** allocXi(int T, int N)
{
    double*** xi;
    xi = new double**[T];
    for (int i = 0; i < T; i++){
        xi[i] = new double* [N];
        for (int j = 0; j < N; j++){
            xi[i][j] = new double[N];
        }
    }
    return xi;
}

void deallocXi(double*** xi, int T, int N)
{
    for (int i = 0; i != T; i++)
    {
        for (int j = 0; j != N; ++j)
        {
            delete[] xi[i][j];
        }
        delete[] xi[i];
    }
    delete [] xi;
}

/*
 *
 * name: BaumWelch
 * @param
 * @return
 * @description
 *
 */
// void HMM::BaumWelch(int T, int *O, double **alpha, double **beta, double **gamma, double ***xi)
void HMM::BaumWelch(int T, int *O, double **alpha, double **beta)
{
    std::cout << "/*BaumWelch Algorithm*/\n";

    HMM temp(N,M);
    temp = this;

    int	l = 0; /*number of interations*/

    double	probf /*Forward probability*/, probb /*Backward probability*/;
    double	numeratorA, denominatorA;
    double	numeratorB, denominatorB;

    double delta, deltaprev, probprev;
    double ratio;

    deltaprev = 10e-70;

    alpha = allocMatrix(T,N);
    beta = allocMatrix(T,N);
    double **gamma = allocMatrix(T,N);
    double ***xi = allocXi(T,N);
    double* scale = new double[T];

    //std::cout << "/* 1. Expectation Preliminaries */\n";
    ForwardScale(T, O, alpha, scale, probf);
    BackwardScale(T, O, beta, scale);
    //Forward(T, O, alpha, probf);
    //Backward(T, O, beta, probb);
    Gamma(T, alpha, beta, gamma);

    //std::cout << "Gamma" << std::endl;
    //print1(gamma,T,N);

    Xi(T, O, alpha, beta, xi);
    //std::cout << "Csi" << std::endl;
    /*for (int t = 0; t < T; ++t) {
        std::cout << "\n\tt = " << t << std::endl;
        for (int i = 0; i < N; ++i){
            for (int j = 0; j < N; ++j){
                std::cout << "\t" << xi[t][i][j];
            }  std::cout << "\n";
        }
    }
    std::cout << std::endl;*/

    probprev = probf;
    //std::cout << "\nprobprev " << probprev << "\n";

    do  {
        //std::cout << "/* 2. Maximization */\n";
        for (int i = 0; i < N; i++)
            temp.pi[i] = gamma[1][i];//.001 + .999*gamma[1][i];

        for (int i = 0; i < N; i++) {
            denominatorA = 0.0;
            for (int t = 0; t < T; t++)
                denominatorA += gamma[t][i];

            for (int j = 0; j < N; j++) {
                numeratorA = 0.0;
                for (int t = 1; t < T - 1; t++)
                    numeratorA += xi[t][i][j];
                temp.A[i][j] = numeratorA/denominatorA;//.001 + .999*numeratorA/denominatorA;
            }

            denominatorB = denominatorA + gamma[T-1][i];
            for (int k = 0; k < M; k++) {
                numeratorB = 0.0;
                for (int t = 0; t < T; t++) {
                    if (O[t] == k)
                        numeratorB += gamma[t][i];
                }

                temp.B[i][k] = numeratorB/denominatorB;//.001 + .999*numeratorB/denominatorB;
            }
        }

        //std::cout << "/* 3. Expectation */\n";
        temp.ForwardScale(T, O, alpha, scale, probf);
        temp.BackwardScale(T, O, beta, scale);
        //temp.Forward(T, O, alpha, probf);
        //temp.Backward(T, O, beta, probb);
        temp.Gamma(T, alpha, beta, gamma);
        temp.Xi(T, O, alpha, beta, xi);

        //std::cout << "\nprobf " << probf;
        delta = probf - probprev;
        //std::cout << "\ndelta " << delta;
        ratio = delta/deltaprev;
       // std::cout << "\nratio " << ratio;
        probprev = probf;
       // std::cout << "\nprobprev " << probprev << "\n";
        deltaprev = delta;
        l++;
        //std::cout << ratio << "\n";
    }
    while (ratio > DELTA);
    //while (l != 200);

    std::cout << "Number of interations: " << l << "\n";
    std::cout << "\n/*Estimated parameters*/\n";
    temp.print();

    deallocXi(xi,T,N);
    deallocMatrix(gamma);
    delete [] scale;
}

/*
 *
 * name: Viterbi
 * @param
 * @return
 * @description
 *
 */
//void HMM::ViterbiLog(int T, int *O, int *q, double **delta, double **psi, double prob)
void HMM::Viterbi(int T, int *O, int *q, double &prob)
{
    int	maxValueIndex = 0;
    double	maxValue = 0.0, value = 0.0;

    double **delta = allocMatrix(T,N);
    double **psi = allocMatrix(T,N);

    /* 1. Initialization  */

    for (int i = 0; i < N; i++) {
        delta[0][i] = pi[i] * (B[i][O[0]]);
        psi[0][i] = 0;
    }

    /* 2. Recursion */

    for (int t = 1; t < T; t++)
    {
        for (int j = 0; j < N; j++)
        {
            maxValue = 0.0;
            maxValueIndex = 1;
            for (int i = 0; i < N; i++)
            {
                value = delta[t-1][i]*(A[i][j]);
                if (value > maxValue)
                {
                    maxValue = value;
                    maxValueIndex = i;
                }
            }

            delta[t][j] = maxValue*(B[j][O[t]]);
            psi[t][j] = maxValueIndex;

        }
    }

    /* 3. Termination */

    prob = 0.0;
    q[T-1] = 1;
    for (int i = 0; i < N; i++)
    {
        if (delta[T-1][i] > prob)
        {
            prob = delta[T-1][i];
            q[T-1] = i;
        }
    }

    /* 4. Path (state sequence) backtracking */

    for (int t = T - 2; t >= 0; t--)
        q[t] = psi[t+1][q[t+1]];

    deallocMatrix(psi);
    deallocMatrix(delta);
}

/*
 *
 * name: ViterbiLog
 * @param
 * @return
 * @description
 *
 */
//void HMM::ViterbiLog(int T, int *O, int *q, double **delta, double **psi, double prob)
void HMM::ViterbiLog(int T, int *O, int *q, double &prob)
{
    int	maxValueIndex = 0;
    double	maxValue = 0.0, value = 0.0;

    double **delta = allocMatrix(T,N);
    double **psi = allocMatrix(T,N);
    double **aux = allocMatrix(N,T);

    /* 0. Preprocessing */

    for (int i = 0; i < N; i++)
        pi[i] = log(pi[i]);

    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++) {
            A[i][j] = log(A[i][j]);
        }

    for (int i = 0; i < N; i++)
        for (int t = 0; t < T; t++) {
            aux[i][t] = log(B[i][O[t]]);
        }

    /* 1. Initialization  */

    for (int i = 0; i < N; i++) {
        delta[0][i] = pi[i] + aux[i][0];
        psi[0][i] = 0;
    }

    /* 2. Recursion */

    for (int t = 1; t < T; t++)
    {
        for (int j = 0; j < N; j++)
        {
            maxValue = -MAXVALUE;
            maxValueIndex = 1;
            for (int i = 0; i < N; i++)
            {
                value = delta[t-1][i] + (A[i][j]);
                if (value > maxValue)
                {
                    maxValue = value;
                    maxValueIndex = i;
                }
            }

            delta[t][j] = maxValue + aux[j][t];
            psi[t][j] = maxValueIndex;

        }
    }

    /* 3. Termination */

    prob = -MAXVALUE;
    q[T-1] = 1;
    for (int i = 0; i < N; i++)
    {
        if (delta[T-1][i] > prob)
        {
            prob = delta[T-1][i];
            q[T-1] = i;
        }
    }

    /* 4. Path (state sequence) backtracking */

    for (int t = T - 2; t >= 0; t--)
        q[t] = psi[t+1][q[t+1]];
}
