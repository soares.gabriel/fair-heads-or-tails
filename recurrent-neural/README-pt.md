 
Classificação de sequências de lançamento de moedas viciadas e não viciadas
===========================================================================
 
 O objetivo do experimento é utilizar um classificador para distinguir entre sequências geradas por moedas viciadas (_biased_) e por moedas não viciadas (_unbiased_). Para isso, na fase de treinamento, são usadas sequências de amostras já classificadas e intercaladas randomicamente para que a rede não adquira um viés.
 
## Conjunto de dados
 
 Os dados são amostras de lançamentos de moedas não viciadas (_unbiased_) e viciadas (_biased_). Foram geradas várias sequências com tamanho fixo e também com tamanho variável.
 
