import logging

logging.basicConfig(
    filename="logs/test.log",
    level=logging.DEBUG,
    format="%(asctime)s:%(levelname)s:%(message)s"
)

import RecurrentNeuralNetwork_pb2 as RNN_pb2

rnnData = RNN_pb2.RecurrentNeuralNetwork()

f = open('rnn.output', 'rb')
rnnData.ParseFromString(f.read())
f.close()