import glob

DATA_PATH = "data/seq_obs_fixed_20_(1k)/"
SEQ_OBS_BIASED = "seq_obs_biased_coin/*"
SEQ_OBS_UNBIASED = "seq_obs_fair_coin/*"

filenames = glob.glob(DATA_PATH+SEQ_OBS_BIASED)

with open(DATA_PATH+'biased', 'w') as outfile:
     for fname in filenames:
         with open(fname) as infile:
             for line in infile:
                 outfile.write(line)

filenames = glob.glob(DATA_PATH+SEQ_OBS_UNBIASED)

with open(DATA_PATH+'unbiased', 'w') as outfile:
     for fname in filenames:
         with open(fname) as infile:
             for line in infile:
                 outfile.write(line)
