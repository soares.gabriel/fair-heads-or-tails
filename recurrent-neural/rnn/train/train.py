import numpy as np
import logging
from tqdm import *

from preprocess import *
import RecurrentNeuralNetwork as RNN
from persistRecurrentNeuralNetwork import persist

logging.basicConfig(
    filename="logs/test.log",
    level=logging.DEBUG,
    format="%(asctime)s:%(levelname)s:%(message)s"
)

BATCH_SIZE = 16
EPOCHS = 300
LEARNING_RATE = 0.0001
MOMENTUM = 0.9
HIDDEN_UNITS = 200
INPUT_UNITS = 12
OUTPUT_UNITS = 2

rnn = RNN.RecurrentNeuralNetwork(in_dim=INPUT_UNITS, h_units=HIDDEN_UNITS, out_dim=OUTPUT_UNITS, batch_size=BATCH_SIZE)


def accuracy(batchs, rnn):
    acc = 0.0
    count = 0.0
    for (i, target) in batchs:
        if len(i) == BATCH_SIZE:
            count += 1.0
            if np.argmax(np.array(target)) == rnn.predict(getRepresentation(i)):
                acc += 1.0

    return acc / count


def error(batchs, rnn):
    count = 0.0
    error = 0.0

    for (i, target) in batchs:
        if len(i) == BATCH_SIZE:
            count += 1.0
            index = np.argmax(np.array(target))
            error += 1.0 * np.log(rnn.predict(getRepresentation(i), returnTuple=True)[index])

    return (-1.0 * error) / count


def training():
    data = Data()
    print('Load BIASED samples')
    logging.info('Load BIASED samples')
    lot_bias = data.buildLots(path=PATH_DATA_TRAIN_BIASED, tam_batch=BATCH_SIZE, target=1)
    print('Load UNBIASED samples')
    logging.info('Load UNBIASED samples')
    lot_unbias = data.buildLots(path=PATH_DATA_TRAIN_UNBIASED, tam_batch=BATCH_SIZE, target=0)
    lots = lot_bias + lot_unbias

    print('Training...')
    logging.info('Training...')
    batch_seq = []
    batch_t = []

    for epoch in tqdm(range(0, EPOCHS)):
        np.random.shuffle(lots)
        for (i, target) in lots:
            if len(i) == BATCH_SIZE:
                targets = [target] * BATCH_SIZE
                if len(batch_seq) < BATCH_SIZE:
                    batch_seq.append(getRepresentation(i))
                    batch_t.append(targets)
                else:
                    rnn.BPTT(batch_seq=batch_seq, batch_target=batch_t,
                             learning_rate=LEARNING_RATE, momentum=MOMENTUM)

                    batch_seq = []
                    batch_t = []

        #print('epoch:', epoch, 'accuracy:', accuracy(lots, rnn), 'error:', error(lots, rnn), flush=True)
        logging.info('epoch: {}, accuracy: {}, error: {}'.format(epoch, accuracy(lots, rnn), error(lots, rnn)))


training()
persist(rnn)
