import logging

logging.basicConfig(
    filename="logs/test.log",
    level=logging.DEBUG,
    format="%(asctime)s:%(levelname)s:%(message)s"
    )

PATH_DATA_TRAIN_BIASED = 'data/biased'
PATH_DATA_TRAIN_UNBIASED = 'data/unbiased'


class Data:
    def __init__(self):
        pass

    def readAllFile(self, path):
        self.arq = open(path)
        self.arq = self.arq.readlines()
        seq = []
        for line in self.arq:
            seq.append(int(line))
        logging.debug("File readed from "+path)
        return seq

    def createLots(self, seq, tam_batch=10, target=1):
        lot = []
        lots = []
        for i in range(0, len(seq)):
            if i % tam_batch == 0:
                lots.append((lot, buildOneHot(index=target)))
                lot = []
                lot.append(seq[i])
            else:
                lot.append(seq[i])
        return lots

    def buildLots(self, path, tam_batch=10, target=1):
        seq = self.readAllFile(path)
        return self.createLots(seq, tam_batch, target)


def buildOneHot(dim=2, index=1):
    vec = [0.0] * dim
    vec[index] = 1.0
    return vec


def buildOneHotInput(dim=4, index=0):
    vec = [0.0] * dim
    vec[index] = 1.0
    return vec


# def intercalate(l1, l2):
#     return [val for pair in zip(l1, l2) for val in pair]
#
# def suffle(batchs):
#     num_changes = 50000
#     qtd = 0
#     while qtd < num_changes:
#         i = randint(0, len(batchs) - 1)
#         j = randint(0, len(batchs) - 1)
#         batchs[i], batchs[j] = batchs[j], batchs[i]
#         qtd += 1
#
# def comp(vec1, vec2):
#     cont = 0
#     for v1 in vec1:
#         for v2 in vec2:
#             if v1 == v2:
#                 cont += 1
#     return cont
#

def getRepresentation(seq):
    ans = []
    for i in range(0, len(seq)):
        ant = seq[i - 1] if (i - 1) >= 0 else 2
        prox = seq[i + 1] if (i + 1) < len(seq) else 3
        ans.append(buildOneHotInput(dim=4, index=ant) +
                   buildOneHotInput(dim=4, index=seq[i]) +
                   buildOneHotInput(dim=4, index=prox))
    return ans
