import numpy as np


def tanh(w):
    return np.tanh(w)

def d_tanh(w):
    return 1.0 - np.tanh(w) ** 2

def softmax(w):
    aux_w = np.exp(w / 1.0)
    return aux_w / np.sum(aux_w)

# logistic
def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))


def d_sigmoid(x):
    return sigmoid(x) * (1.0 - sigmoid(x))
