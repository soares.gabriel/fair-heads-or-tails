import logging

logging.basicConfig(
    filename="logs/test.log",
    level=logging.DEBUG,
    format="%(asctime)s:%(levelname)s:%(message)s"
)

import RecurrentNeuralNetwork_pb2 as RNN_pb2

def serializeArray(array, peristArray):
    index = 0
    for i in array:
        peristArray.add()
        for j in i:
            peristArray[index].data.append(j)
        index += 1

def persist(rnn):
    persistRNN = RNN_pb2.RecurrentNeuralNetwork()

    persistRNN.in_dim = rnn.in_dim
    persistRNN.out_dim = rnn.out_dim
    persistRNN.h_units = rnn.h_units
    persistRNN.batch_size = rnn.batch_size

    serializeArray(rnn.U, persistRNN.U)
    serializeArray(rnn.W, persistRNN.W)
    serializeArray(rnn.V, persistRNN.V)

    serializeArray(rnn.S, persistRNN.S)
    serializeArray(rnn.O, persistRNN.O)
    serializeArray(rnn.A, persistRNN.A)
    serializeArray(rnn.B, persistRNN.B)

    # Write the new RNN components to disk
    f = open('rnn.output', 'wb')
    f.write(persistRNN.SerializeToString())
    f.close()