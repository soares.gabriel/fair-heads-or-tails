import numpy as np
import ActivationFunctions as activation_function
import logging

logging.basicConfig(
    filename="logs/test.log",
    level=logging.DEBUG,
    format="%(asctime)s:%(levelname)s:%(message)s"
    )

class RecurrentNeuralNetwork:
    def __init__(self, in_dim, h_units, out_dim, batch_size):
        self.out_dim = out_dim
        self.h_units = h_units
        self.in_dim = in_dim
        self.batch_size = batch_size
        self.initParameters()

    def initParameters(self):

        self.basicWeightInit()

        self.S = [np.zeros(self.h_units, dtype=float)] * (self.batch_size + 1)
        self.O = [np.zeros(self.out_dim, dtype=float)] * (self.batch_size + 1)
        self.A = [np.zeros(self.h_units, dtype=float)] * (self.batch_size + 1)
        self.B = [np.zeros(self.out_dim, dtype=float)] * (self.batch_size + 1)

        self.ant_dU = np.zeros((self.h_units, self.in_dim), dtype=float)
        self.ant_dW = np.zeros((self.h_units, self.h_units), dtype=float)
        self.ant_dV = np.zeros((self.out_dim, self.h_units), dtype=float)

    def basicWeightInit(self):
        self.U = np.random.normal(scale=0.1, size=(self.h_units, self.in_dim))
        self.W = np.random.normal(scale=0.1, size=(self.h_units, self.h_units))
        self.V = np.random.normal(scale=0.1, size=(self.out_dim, self.h_units))

    def standardWeightInitTanh(self):
        self.U = np.random.uniform(-np.sqrt(1. / self.in_dim),
                                   np.sqrt(1. / self.in_dim),
                                   (self.h_units, self.in_dim))
        self.W = np.random.uniform(-np.sqrt(1. / self.h_units),
                                   np.sqrt(1. / self.h_units),
                                   (self.h_units, self.h_units))
        self.V = np.random.uniform(-np.sqrt(1. / self.h_units),
                                   np.sqrt(1. / self.h_units),
                                   (self.out_dim, self.h_units))

    def normalizedWeightInitTanh(self):
        self.U = np.random.uniform(-np.sqrt(6 / (self.in_dim + self.h_units)),
                                   np.sqrt(6 / (self.in_dim + self.h_units)),
                                   (self.h_units, self.in_dim))
        self.W = np.random.uniform(-np.sqrt(6 / (self.h_units + self.h_units)),
                                   np.sqrt(6 / (self.h_units + self.h_units)),
                                   (self.h_units, self.h_units))
        self.V = np.random.uniform(-np.sqrt(6 / (self.h_units + self.out_dim)),
                                   np.sqrt(6 / (self.h_units + self.out_dim)),
                                   (self.out_dim, self.h_units))

    def normalizedWeightInitSigmoid(self):
        self.U = np.random.uniform(-4*np.sqrt(6 / (self.in_dim + self.h_units)),
                                   4*np.sqrt(6 / (self.in_dim + self.h_units)),
                                   (self.h_units, self.in_dim))
        self.W = np.random.uniform(-4*np.sqrt(6 / (self.h_units + self.h_units)),
                                   4*np.sqrt(6 / (self.h_units + self.h_units)),
                                   (self.h_units, self.h_units))
        self.V = np.random.uniform(-4*np.sqrt(6 / (self.h_units + self.out_dim)),
                                   4*np.sqrt(6 / (self.h_units + self.out_dim)),
                                   (self.out_dim, self.h_units))


    def resetState(self):
        self.S = [np.zeros(self.h_units, dtype=float)] * (self.batch_size + 1)

    def forward(self, x, t):
        self.A[t] = self.W.dot(self.S[t - 1]) + self.U.dot(x).reshape(self.h_units)
        self.S[t] = activation_function.tanh(self.A[t])

        self.B[t] = self.V.dot(self.S[t])
        self.O[t] = activation_function.softmax(self.B[t])

    def forwardSequence(self, seq):
        for t in range(0, len(seq)):
            self.forward(seq[t], t)
        self.SUM = np.sum(np.array(self.S), axis=0)
        L = len(seq) - 1
        self.B[L] = self.V.dot(self.SUM)
        self.O[L] = activation_function.softmax(self.B[L])

    def predict(self, seq, returnTuple=False):
        self.forwardSequence(seq)
        if returnTuple:
            return self.O[len(seq) - 1]
        else:
            return np.argmax(self.O[len(seq) - 1])

    def getErrors(self, seq, targets):
        self.resetState()
        self.forwardSequence(seq)
        self.targets = targets
        self.seq = seq

        dU = np.zeros((self.h_units, self.in_dim), dtype=float)
        dW = np.zeros((self.h_units, self.h_units), dtype=float)
        dV = np.zeros((self.out_dim, self.h_units), dtype=float)

        t = len(seq)

        for t in range(t - 1, t)[::-1]:
            adV, adW, adU = self.grad(t)
            dU += adU
            dW += adW
            dV += adV

        return dU, dW, dV

    def BPTT(self, batch_seq, batch_target, learning_rate=0.0001, momentum=0.9):

        dU = np.zeros((self.h_units, self.in_dim), dtype=float)
        dW = np.zeros((self.h_units, self.h_units), dtype=float)
        dV = np.zeros((self.out_dim, self.h_units), dtype=float)

        for i in range(0, len(batch_seq)):
            adU, adW, adV = self.getErrors(batch_seq[i], batch_target[i])
            dU += adU
            dW += adW
            dV += adV

        at_dU = momentum * self.ant_dU + learning_rate * dU
        at_dW = momentum * self.ant_dW + learning_rate * dW
        at_dV = momentum * self.ant_dV + learning_rate * dV

        self.U -= at_dU
        self.W -= at_dW
        self.V -= at_dV

        self.ant_dU = at_dU
        self.ant_dU = at_dU
        self.ant_dU = at_dU

    def grad(self, t):
        delta = self.O[t] - self.targets[t]
        dv = np.outer(delta, self.SUM)
        delta = delta.dot(self.V)
        aux = activation_function.d_tanh(self.A[t])
        delta = delta.dot(np.diag(aux))
        dw = np.outer(delta, self.S[t - 1])
        du = np.outer(delta, self.seq[t])
        delta = np.asarray(delta)

        for i in range(0, t)[::-1]:
            delta = np.asarray(delta)
            delta = delta.dot(self.W)
            aux = activation_function.d_tanh(self.A[i])
            delta = delta.dot(np.diag(aux))
            dw += np.outer(delta, self.S[i - 1])
            du += np.outer(delta, self.seq[i])

        return dv, dw, du
