package com.example.cointossing;

/* Java libary to handle lightweight, self-contained mathematics and statistics components */
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

/* Java libary to handle tables like the Pandas python library DataFrame */
import com.dataframe.CSV;
import com.dataframe.DataFrame;

public class Main {

    public static void main(String[] args) {

        DataFrame data_frame = new CSV("data/data_1000_12.csv", ",").read();
        loadRecurrentNeuralNetwork lr = new loadRecurrentNeuralNetwork();
        RecurrentNeuralNetwork rnn = new RecurrentNeuralNetwork(lr);

        double[] tmp = rnn.predict(data_frame.data);
        for (int i = 0; i < tmp.length; i++)
            System.out.println(tmp[i]);
    }
}
