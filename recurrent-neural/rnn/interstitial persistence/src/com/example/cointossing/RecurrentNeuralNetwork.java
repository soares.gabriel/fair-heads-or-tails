package com.example.cointossing;

import org.apache.commons.math3.linear.*;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.analysis.function.*;

import java.lang.reflect.Array;


public class RecurrentNeuralNetwork {

    int out_dim;
    int h_units;
    int in_dim;
    int batch_size;

    RealMatrix U;
    RealMatrix W;
    RealMatrix V;

    RealMatrix S;
    RealMatrix O;
    RealMatrix A;
    RealMatrix B;


    public RecurrentNeuralNetwork(int inp_dim, int hid_units, int outp_dim, int lbatch_size) {

        out_dim = outp_dim;
        h_units = hid_units;
        in_dim = inp_dim;
        batch_size = lbatch_size;

        U = MatrixUtils.createRealMatrix(h_units, in_dim);
        W = MatrixUtils.createRealMatrix(h_units, h_units);
        V = MatrixUtils.createRealMatrix(out_dim, h_units);

        S = MatrixUtils.createRealMatrix(h_units, batch_size + 1);
        O = MatrixUtils.createRealMatrix(out_dim, batch_size + 1);
        A = MatrixUtils.createRealMatrix(h_units, batch_size + 1);
        B = MatrixUtils.createRealMatrix(out_dim, batch_size + 1);
    }

    public RecurrentNeuralNetwork(loadRecurrentNeuralNetwork lr) {

        out_dim = lr.rnnpb2.getOutDim();
        h_units = lr.rnnpb2.getHUnits();
        in_dim = lr.rnnpb2.getInDim();
        batch_size = lr.rnnpb2.getBatchSize();

        U = MatrixUtils.createRealMatrix(h_units, in_dim);
        for (int i = 0; i < lr.rnnpb2.getUCount(); i++) {
            for (int j = 0; j < lr.rnnpb2.getUList().get(i).getDataList().size(); j++) {
                U.setEntry(i,j,lr.rnnpb2.getUList().get(i).getData(j));
                //System.out.print(lr.rnnpb2.getUList().get(i).getData(j)+" ");
            }
            //System.out.println();
        }

        W = MatrixUtils.createRealMatrix(h_units, h_units);
        for (int i = 0; i < lr.rnnpb2.getWCount(); i++) {
            for (int j = 0; j < lr.rnnpb2.getWList().size(); j++) {
                W.setEntry(i,j,lr.rnnpb2.getWList().get(i).getData(j));
                //System.out.print(lr.rnnpb2.getWList().get(i).getData(j)+" ");
            }
            //System.out.println();
        }

        V = MatrixUtils.createRealMatrix(out_dim, h_units);
        for (int i = 0; i < lr.rnnpb2.getVCount(); i++) {
            for (int j = 0; j < lr.rnnpb2.getVList().get(i).getDataList().size(); j++) {
                V.setEntry(i,j, lr.rnnpb2.getVList().get(i).getData(j));
                //System.out.print(lr.rnnpb2.getVList().get(i).getData(j)+" ");
            }
            //System.out.println();
        }

        S = MatrixUtils.createRealMatrix(batch_size + 1, h_units);
        for (int i = 0; i < lr.rnnpb2.getSCount(); i++) {
            for (int j = 0; j < lr.rnnpb2.getSList().get(i).getDataList().size(); j++) {
                S.setEntry(i, j, lr.rnnpb2.getSList().get(i).getData(j));
                //System.out.print(lr.rnnpb2.getSList().get(i).getData(j)+" ");
            }
            //System.out.println();
        }

        O = MatrixUtils.createRealMatrix(batch_size + 1, out_dim);
        for (int i = 0; i < lr.rnnpb2.getOCount(); i++) {
            for (int j = 0; j < lr.rnnpb2.getOList().get(i).getDataList().size(); j++) {
                O.setEntry(i, j, lr.rnnpb2.getOList().get(i).getData(j));
                //System.out.print(lr.rnnpb2.getOList().get(i).getData(j)+" ");
            }
            //System.out.println();
        }

        A = MatrixUtils.createRealMatrix(batch_size + 1, h_units);
        for (int i = 0; i < lr.rnnpb2.getACount(); i++) {
            for (int j = 0; j < lr.rnnpb2.getAList().get(i).getDataList().size(); j++) {
                A.setEntry(i, j, lr.rnnpb2.getAList().get(i).getData(j));
                //System.out.print(lr.rnnpb2.getAList().get(i).getData(j)+" ");
            }
            //System.out.println();
        }

        B = MatrixUtils.createRealMatrix(batch_size + 1, out_dim);
        for (int i = 0; i < lr.rnnpb2.getBCount(); i++) {
            for (int j = 0; j < lr.rnnpb2.getBList().get(i).getDataList().size(); j++) {
                B.setEntry(i, j, lr.rnnpb2.getBList().get(i).getData(j));
                //System.out.print(lr.rnnpb2.getBList().get(i).getData(j)+" ");
            }
            //System.out.println();
        }
    }

    /*
        argMax function from:
        https://github.com/mrgimo/mixtape-core/blob/master/src/main/java/ch/hsr/mixtape/util/MathUtils.java
    */
    public static int argMax(double... values) {
        return argMax(values, 0, values.length);
    }

    public static int argMax(double[] values, int from, int to) {
        int argMax = from;
        for (int i = from + 1; i < to; i++)
            if (values[argMax] < values[i])
                argMax = i;

        return argMax;
    }

    public RealVector tanh(RealVector w) {
        return w.map(new Tanh());
    }

    public RealVector softmax(RealVector w) {
        RealVector aux_w = w.map(new Exp());
        return aux_w.mapDivideToSelf(StatUtils.sum(aux_w.toArray()));
    }

    public void forward(RealVector x, int t){

        RealVector tmp = U.operate(x);
        A.setRow(t, W.operate(S.getRowVector(t - 1)).add(tmp).toArray());
    
        S.setRow(t, tanh(A.getRowVector(t)).toArray());

        B.setRow(t, V.operate(S.getRowVector(t)).toArray());
        O.setRow(t, softmax(B.getRowVector(t)).toArray());
    }

    public void forwardSequence(RealMatrix seq) {
        for(int t = 0; t < seq.getRowDimension(); t++){
            forward(seq.getRowVector(t), t + 1);
        }

        double[] sum = new double[S.getColumnDimension()];
        double tmp = 0;
        for(int i = 0; i < S.getColumnDimension(); i++){
            for(int j = 0; j < S.getRowDimension(); j++) {
                tmp += S.getEntry(j, i);
            }
            sum[i] = tmp;
            tmp = 0;
        }

        int l = seq.getRowDimension() - 1;
        B.setRow(l, V.operate(sum));
        O.setRow(l, softmax(B.getRowVector(l)).toArray());
    }

    public double[] predict(RealMatrix seq){
        forwardSequence(seq);
        return O.getData()[seq.getRowDimension() - 1];
    }

}

